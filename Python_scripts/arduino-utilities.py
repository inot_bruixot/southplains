#!/usr/bin/python3

#import socket
import time
import epics
import arpreq
from datetime import datetime
import os 
import subprocess


# Time Variables
period_health =1000;
startMillis_health=0;


# Environment Variables
Project="BREWERY"
Tank_201="Tank1"
Tank_202="Tank2"
Tank_204="Tank4"
Propierty="HTH"


# UDP Devices 
ip_tank_201 = "192.168.1.21"
ip_tank_202= "192.168.1.22"
ip_tank_204 = "192.168.1.24"
#port_tank = 4210

# UPD Nomenclature
#tmp_request="*TMP?"


# UPD Commands
#udp_Heater_CMD =0
#udp_Heater_CMD_old =0




# Socket definition
#sock = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)



milliseconds = int(round(time.time() * 1000))
print(milliseconds)

#test=0

#hostname = "192.168.1.223" #example
#response = os.system("ping -c 1 " + hostname)
#response = os.system("ping -c 1 " + hostname)
#print ("hellooooo")
#print (response)

#try:
#    response = subprocess.check_output(
#        ['ping', '-c', '1', '192.168.1.221'],
#        stderr=subprocess.STDOUT,  # get all output
#        universal_newlines=True  # return string not bytes
#    )
#except subprocess.CalledProcessError:
#    response = None
#    print ("device not detected")



#print("here fast")

#ip = '192.168.0.10'
#if os.system("ping -n 1 -w 5 " + ip) == 0:
#    print ("OK")
#else:
#    print ("NOK")

#online = os.system("ping -n 1 192.168.0.9")

print("here fast")



while True:
	
	time.sleep(30)

	# REQUEST DATA
	currentMillis_health = int(round(time.time() * 1000))
	#test whether the period has elapsed
	if (currentMillis_health - startMillis_health >= period_health):

		now = datetime.now()
		hora = now.strftime("%H") + now.strftime("%M")+now.strftime("%S")
		hora_bytes = bytes(hora, 'utf-8')
		

		startMillis_health = currentMillis_health;  #IMPORTANT to save the start time of the current LED state.

		#print(arpreq.arpreq(ip_tank) is None)
		
	#try:
    #		response = subprocess.check_output(
    #   	['ping', '-c', '1', '192.168.1.201'],
    #    	#epics.caput(Project+':'+Tank_201+':'+Propierty,1)
    #    	#stderr=subprocess.STDOUT,  # get all output
    #    	#universal_newlines=True  # return string not bytes
    # 	)
	#except subprocess.CalledProcessError:
    #		response = None
    #		#print ("Not detected")
   	#		print (hora)
    #		#epics.caput(Project+':'+Tank_201+':'+Propierty,1)
    #		print ("192.168.1.201 device not detected")

	
		if (arpreq.arpreq(ip_tank_201) is None):
			#epics.caput(P+':'+D2+':'+'HTH',0)		
		#	#ec		
			epics.caput(Project+':'+Tank_201+':'+Propierty,0)
			#print("ttttrue")
			print (hora+' // '+ip_tank_201+' is not online')
	
		else:
			#epics.caput(P+':'+D2+':'+'HTH',1)		
			#epics.caput('BWY:Heater:HTH.VAL','0')		
			epics.caput(Project+':'+Tank_201+':'+Propierty,1)
			#print(ip_tank_201+' is online')

		if (arpreq.arpreq(ip_tank_202) is None):
			#epics.caput(P+':'+D2+':'+'HTH',0)		
			#epics.caput('BWY:Heater:HTH.VAL','1')		
			epics.caput(Project+':'+Tank_202+':'+Propierty,0)
			#print("ttttrue")
			print (hora+' // '+ip_tank_202+' is not online')
		else:
			#epics.caput(P+':'+D2+':'+'HTH',1)		
			#epics.caput('BWY:Heater:HTH.VAL','0')		
			epics.caput(Project+':'+Tank_202+':'+Propierty,1)
			#print(ip_tank_201+' is online')

		if (arpreq.arpreq(ip_tank_204) is None):
			#epics.caput(P+':'+D2+':'+'HTH',0)		
			#epics.caput('BWY:Heater:HTH.VAL','1')		
			epics.caput(Project+':'+Tank_204+':'+Propierty,0)
			#print("ttttrue")
			print (hora+' // '+ip_tank_204+' is not online')
		else:
			#epics.caput(P+':'+D2+':'+'HTH',1)		
			#epics.caput('BWY:Heater:HTH.VAL','0')		
			epics.caput(Project+':'+Tank_204+':'+Propierty,1)
			#print(ip_tank_201+' is online')

	#	print('request:'+str(currentMillis_request))
	#	sock.sendto(bytes(tmp_request, 'utf-8'),(ip_tank,4210))
	#	sock.sendto(bytes("*HTR?", 'utf-8'),(ip_tank,4210))

	
