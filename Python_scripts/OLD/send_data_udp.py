#!/usr/bin/python3.8


import socket
import time
import epics


# Time Variables
period_request =3000;
period_push=2000;
startMillis_push=0;
startMillis_request=0;


# Environment Variables
P="BWY"
D1="Tank"
D2="Heater"

# UDP Devices 
ip_tank = "192.168.1.177"
port_tank = 4210

# UPD Nomenclature
tmp_request="*TMP?"
cmd_status_request="*HTR?"
target_tmp_command="*TTP!"
auto_command="*AUT"



# UDP Commands
udp_Heater_CMD =0
udp_Heater_CMD_old =0

udp_Target_TMP =0
udp_Target_TMP_old=0

udp_Auto_CMD=0
udp_Auto_CMD_old=0





# Socket definition
sock = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)



#milliseconds = int(round(time.time() * 1000))
#print(milliseconds)

test=0

while True:
	

	# REQUEST DATA
	currentMillis_request = int(round(time.time() * 1000))
	#test whether the period has elapsed
	if (currentMillis_request - startMillis_request >= period_request):

		startMillis_request = currentMillis_request;  #IMPORTANT to save the start time of the current LED state.

		print('request:'+str(currentMillis_request))
		sock.sendto(bytes(tmp_request, 'utf-8'),(ip_tank,4210))
		sock.sendto(bytes(cmd_status_request, 'utf-8'),(ip_tank,4210))




	
	
	# PUSH DATA
	currentMillis_push = int(round(time.time() * 1000))
	if (currentMillis_push - startMillis_push >= period_push):
	
		print('push:'+str(currentMillis_push))	
		startMillis_push = currentMillis_push;  #IMPORTANT to save the start time of the current LED state

		udp_Heater_CMD=epics.caget(P+':'+D2+':'+'CMD.VAL')
		if (udp_Heater_CMD!=udp_Heater_CMD_old):
			sock.sendto(bytes("*HTR!="+str(udp_Heater_CMD), 'utf-8'),(ip_tank,4210))
			print(udp_Heater_CMD)


		udp_Heater_CMD_old = udp_Heater_CMD
	

	
		udp_Target_TMP=epics.caget(P+':'+D2+':'+'TTP.VAL')
		if (udp_Target_TMP!=udp_Target_TMP_old):
			sock.sendto(bytes("*TTP!="+str(udp_Target_TMP), 'utf-8'),(ip_tank,4210))
			print(udp_Target_TMP)

		udp_Target_TMP_old = udp_Target_TMP



		udp_Auto_CMD=epics.caget(P+':'+D2+':'+'AUT.VAL')
		if (udp_Auto_CMD!=udp_Auto_CMD_old):
			sock.sendto(bytes("*AUT!="+str(udp_Auto_CMD), 'utf-8'),(ip_tank,4210))
			print(udp_Auto_CMD)

		udp_Auto_CMD_old = udp_Auto_CMD
