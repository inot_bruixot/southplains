#!/usr/bin/python3.8


import socket
import epics
import time
from datetime import datetime


# EPICS Environment Variable
P="BWY"
D1="Tank"
D2="Heater"


# Socket Definition
sock = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)

print("try")

# Computer Definition
pc_addr=("192.168.1.131",4210)
# Binding of the Address with the computer
sock.bind(pc_addr)

esp8266_addr = ("192.168.1.211",4022)





# COMMAND

tmp_data = 0
htr_data =0


while True:

	#time.sleep(30)

	sPacket,pc_addr=sock.recvfrom(25)
	print (sPacket)
	print (pc_addr)

	
	pos_star=str(sPacket).find("*")
	pos_equal= str(sPacket).find("=")
	packetSize=len(str(sPacket))

	
	data = str(sPacket)[pos_star+1:pos_equal]
	value = str(sPacket)[pos_equal+1:packetSize-1]

	#print(P+':'+D1+':'+'TMP.VAL')
	if (data=="TMP?"):
		#epics.caput(P+':'+D1+':'+'TMP.VAL',value)
		print("Temp arrived")
		sock.sendto(bytes(str(tmp_data), 'utf-8'),(esp8266_addr))
		print("Data sent back :" +str(tmp_data))
		tmp_data=tmp_data +1

		now = datetime.now()
		hora = now.strftime("%H") + now.strftime("%M")+now.strftime("%S.%f")
		hora_bytes = bytes(hora, 'utf-8')
		print("TIME  :" +str(hora_bytes))
		

	if (data=="HTR?"):
		#epics.caput(P+':'+D2+':'+'RBK.VAL',value)
		print("HTR arrived")
		sock.sendto(bytes(str(htr_data), 'utf-8'),(esp8266_addr))
		print("Data sent back:" +str(htr_data))

		now = datetime.now()
		hora = now.strftime("%H") + now.strftime("%M")+now.strftime("%S.%f")
		hora_bytes = bytes(hora, 'utf-8')
		print("TIME  :" +str(hora_bytes))



	if (tmp_data>99):
		tmp_data=0


	if (htr_data==0):
		htr_data=1
	else: 
		htr_data=0










	#if (data=="HTR!"):
	#	epics.caput(P+':'+D2+':'+'CMD.VAL',value)




	#currentMillis = int(round(time.time() * 1000))

	#test whether the period has elapsed
	#if (currentMillis - startMillis >= period):

	#	startMillis = currentMillis;  #IMPORTANT to save the start time of the current LED state.

	#	print(currentMillis)
		
	# (test<100):
	#	epics.caput('hpE3631A:Test:TESTTMP.VAL',test)
	#	test=test+1;
	#else:
#		test=0



