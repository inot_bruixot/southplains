import dash
from dash.dependencies import Input, Output
import dash_html_components as html
import dash_core_components as dcc
import dash_daq as daq
import plotly.graph_objects as go
from random import randrange
import epics



external_stylesheets = ["https://codepen.io/chriddyp/pen/bWLwgP.css"]
#app = dash.Dash(__name__, external_stylesheets=external_stylesheets, update_title=None)
app = dash.Dash(__name__, assets_folder = 'assets', include_assets_files = True, external_stylesheets=external_stylesheets, update_title=None)
#app = dash.Dash(__name__, assets_folder = 'assets', include_assets_files = True, external_stylesheets=external_stylesheets)
#app.layout = html.Div(style={
#‘background-image’: ‘url(“diginex.png”)’,
#‘background-repeat’: ‘no-repeat’,
#‘background-position’: ‘right top’,
#‘background-size’: ‘150px 100px’
#},
#app = dash.Dash(__name__, update_title=None)

colors = {
    'background': "#000000",
    'text': "#FFFFFF",
    'led': "#93cbf0",
}

# *************************************************************************
app.layout = html.Div(style={'backgroundColor': colors['background']},
    id="dark-light-theme",
    children=[
        html.Div(
            [
                html.H1("Southplains Brewery", style={"textAlign": "center",'color': colors['text']}),
                #html.H2("Water Tank Control", style={"textAlign": "left",'color': colors['text']}),
                html.Br(),
            #[
                #html.Div(
                #    daq.Tank(
                #        id="my-tank",
                #        max=400,
                #        value=197,
                #        showCurrentValue=True,
                #        units="gallons",
                #        style={"margin-left": "50px"},
                #    ),
                #    className="three columns",
                #),
                html.Div(

                	html.Table(
        #			# Header
        			#[html.Tr([html.Tr("First", style={"textAlign": "center",'color': colors['text']}) #for col in dataframe.columns
        			#	])] #+
			        #
                    [html.Tr(
                        [
                        #for col in dataframe.columns
            #            html.Div([dcc.RangeSlider(
            #id='condition-range-slider',
            #min=0,
            #max=30,
            #value=[10, 15],
            #allowCross=False)],
            #style={'width': '50%'}
        #),

            #            html.Td(
            #                daq.Indicator(
            #                id="Heater-HTH",size=100, color=colors['led'],
            #                label={'label': 'Heater Health','style': {'color': colors['text'],'fontSize':40}},
            #                ),  
            #            ) #for
                        ])
                        ] #+
                    +

			        [html.Tr(
			        	[
			#        	html.Td(
			#        		daq.ToggleSwitch(
            #        		id="Heater-CMD", 
            #        		label={'label': 'Heater [Off / On]','style': {'color': colors['text'],'fontSize':40}},
            #        		size="200",#value=True
            #    			),
			#        	), #for col in dataframe.columns
        				html.Td(
			        		daq.Indicator(
                        	id="Heater-Status",size=100, color=colors['led'],
                        	label={'label': 'Heater Status','style': {'color': colors['text'],'fontSize':40}},
                    		),  
			        	) #for
        				]
                        )] #+
			        +
			        [html.Tr(
			        	[
			        	html.Td(
			        		daq.Gauge(
                        	#style={'color':colors['text']},
                        	#color={"gradient":True,"ranges":{"blue":[-10,0],"green":[0,60],"yellow":[60,80],"red":[80,100]}},
                            color={"gradient":True,"ranges":{"green":[0,60],"yellow":[60,80],"red":[80,100]}},
                        	size=400,                        	     
                        	showCurrentValue=True,    
                        	#units="°C",             
                        	id="Temperature-gauge",
                        	label={'label': 'Tank 1 ','style': {'color': colors['text'],'fontSize':40}},
                        	scale={'labelInterval':1, 'style': {'color': colors['text'],'fontSize':80}},
                        	#custom={'style': {'color': colors['text'],'fontSize':80}},
                        	min=0, max=100 ,
                        	style={"textAlign": "left",'color': colors['text'],'margin-top':40,'height':'80%'},
                    		),
			        	), 
                        html.Td(
                            daq.Gauge(
                            #style={'color':colors['text']},
                            color={"gradient":True,"ranges":{"green":[0,60],"yellow":[60,80],"red":[80,100]}},
                            #color={"gradient":True,"ranges":{"blue":[-10,0],"green":[0,60],"yellow":[60,80],"red":[80,100]}},
                            size=400,                                
                            showCurrentValue=True,    
                            #units="°C",             
                            id="Temperature-gauge2",
                            label={'label': 'Tank 2','style': {'color': colors['text'],'fontSize':40}},
                            scale={'labelInterval':1, 'style': {'color': colors['text'],'fontSize':80}},
                            #custom={'style': {'color': colors['text'],'fontSize':80}},
                            min=0, max=100 ,
                            style={"textAlign": "left",'color': colors['text'],'margin-top':40,'height':'80%'},
                           ),
                        ),
                        html.Td(
                            daq.Gauge(
                            #style={'color':colors['text']},
                            #color={"gradient":True,"ranges":{"blue":[-10,0],"green":[0,60],"yellow":[60,80],"red":[80,100]}},
                            color={"gradient":True,"ranges":{"green":[0,60],"yellow":[60,80],"red":[80,100]}},
                            size=400,                                
                            showCurrentValue=True,    
                            #units="°C",             
                            id="Temperature-gauge4",
                            label={'label': 'Tank 4','style': {'color': colors['text'],'fontSize':40}},
                            scale={'labelInterval':1, 'style': {'color': colors['text'],'fontSize':80}},
                            #custom={'style': {'color': colors['text'],'fontSize':80}},
                            min=0, max=100 ,
                            style={"textAlign": "left",'color': colors['text'],'margin-top':40,'height':'80%'},
                           ),

                        ),
                    #    html.Td(
 
                    #    )

        				#])], #+
                        ])
                        ] #+
                    +
                    [html.Tr(
                        [
                        html.Td(
                            daq.Indicator(
                            id="Tank1-HTH",size=100, color=colors['led'],
                            label={'label': 'Tank1-HTH','style': {'color': colors['text'],'fontSize':40}},
                            )  
                        
                        ),
                        html.Td(
                            daq.Indicator(
                            id="Tank2-HTH",size=100, color=colors['led'],
                            label={'label': 'Tank2-HTH','style': {'color': colors['text'],'fontSize':40}},
                            )  
                        
                        ),
                        html.Td(
                            daq.Indicator(
                            id="Tank3-HTH",size=100, color=colors['led'],
                            label={'label': 'Tank3-HTH','style': {'color': colors['text'],'fontSize':40}},
                            )  
                        
                        ),
                        ])
                        ], #+

        			),
                ),

                html.Div( 

                ),
                html.Div(
                
                ),
                html.Div(

                ),

            ],
            className="row",
        ),
        #html.Div(
        #    html.Div(
        #        daq.ToggleSwitch(
        #            id="my-toggle-switch", label="OFF | ON", size="100"#value=True
        #        ),
        #        className="three columns",
        #    ),
        #    className="row",
        #),
        #html.Div(
        #    dcc.Graph(id="my-graph", figure={}),
        #    className="row",
        #),
        dcc.Interval(id="timing", interval=500, n_intervals=0),
        html.Br(),
        html.Br(),
        html.Br(),
        html.Br(),
        html.Br(),
      	html.Br(),
        html.Br(),
        html.Br(),
        html.Br(),
        html.Br(),
         html.Br(),
        html.Br(),
        html.Br(),
        html.Br(),
        html.Br(),
      	html.Br(),
        html.Br(),
        html.Br(),
        html.Br(),
        html.Br(),
          
    ],
)




# *************************************************************************
# must have Dash 1.16.0 or higher for this to work
@app.callback(
    Output("Temperature-gauge", "value"),
    Output("Temperature-gauge2", "value"),
    #Output("Temperature-gauge3", "value"),
    Output("Temperature-gauge4", "value"),

    #Output("my-thermometer", "value"),
    
    Output("Heater-Status","value"),
    Output("Heater-Status","color"),
    #Output("Heater-HTH","value"),
    #Output("Heater-HTH","color"),
    
    #Output("my-graph", "figure"),
    Input("timing", "n_intervals"),
)
def update_g(n_intervals):
    
    #data_epics = epics.caget('MARTINI:Tank:TMP.VAL')
    #pressure_1 = epics.caget('MARTINI:Tank:TMP.VAL')
    #pressure_1 = randrange(100)  # mimics data pulled from live database
    
    #Temperature = epics.caget('BWY:Tank:TMP.VAL')  # mimics data pulled from live database
    #heater_status = epics.caget('BWY:Heater:RBK')
    #Heater_HTH = epics.caget('BWY:Heater:HTH')

    Temp_201 = epics.caget('BREWERY:Tank1:TMP.VAL')  # mimics data pulled from live database
    Temp_202 = epics.caget('BREWERY:Tank2:TMP.VAL')
    #Temp_203 = epics.caget('BREWERY:Tank3:IDN.VAL')
    Temp_204 = epics.caget('BREWERY:Tank4:TMP.VAL')
    #Temp_202 = epics.caget('BREWERY:Tank2:IDN.VAL')

#    #heater_status = epics.caget('BREWERY:Tank1:RST')
    Heater_HTH = epics.caget('BREWERY:Tank4:HTH')

    if (Heater_HTH):
        Heater_HTH_color = "#BA0303"
    else:
        Heater_HTH_color = "#35BA03"

    
    
    #return Temp_201, heater_status, Heater_HTH, Heater_HTH_color
    #return Temp_201, Temp_202, heater_status, Heater_HTH, Heater_HTH_color
    return Temp_201, Temp_202, Temp_204,Heater_HTH,Heater_HTH_color





#@app.callback(
    #Output("my-tank", "units"),
    #Output("Heater-CMD","color"),
    #Input("Heater-CMD", "value"),

#)
#def update_g(toggle):
#    if toggle:
#        #epics.caput('BWY:Heater:CMD',1)
#        epics.caput('BREWERY:Tank1:RST',1)
#        color = "#93cbf0"
#        #return "On"
#        return color
#    else:
#        epics.caput('BREWERY:Tank1:RST',0)
#        color = False
#        return color
  



# AUTO SWITCH

#@app.callback(
#    Output('LED-display', 'value'),
#    Input('Temperature-Slider', 'value'))
#def update_output(value):
#    epics.caput('BWY:Heater:TTP',value)
#    return value
        

# AUTO SWITCH 

#@app.callback(
#    #Output("my-tank", "units"),
#    Output("Heater-AUT","color"),
#    Input("Heater-AUT", "value"),
#)
#def update_g(toggle):
#    if toggle:
#        epics.caput('BWY:Heater:AUT',1)
#        color = "#93cbf0"

#        return color
#    else:
#        epics.caput('BWY:Heater:AUT',0)
#        color = False
#        return color
 



if __name__ == "__main__":
    #app.run_server(debug=True, port=3030)
    app.run_server(debug=True,host="0.0.0.0", port=3030)