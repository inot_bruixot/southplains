import dash
from dash.dependencies import Input, Output
#import dash_html_components as html
from dash import html
#import dash_core_components as dcc
from dash import dcc
import dash_daq as daq
import plotly.graph_objects as go
from random import randrange
import epics
import dash_bootstrap_components as dbc



external_stylesheets = ["https://codepen.io/chriddyp/pen/bWLwgP.css"]
#app = dash.Dash(__name__, external_stylesheets=external_stylesheets, update_title=None)
app = dash.Dash(__name__, assets_folder = 'assets', include_assets_files = True, external_stylesheets=external_stylesheets, update_title=None)
#app = dash.Dash(__name__, assets_folder = 'assets', include_assets_files = True, external_stylesheets=external_stylesheets)
#app.layout = html.Div(style={
#‘background-image’: ‘url(“diginex.png”)’,
#‘background-repeat’: ‘no-repeat’,
#‘background-position’: ‘right top’,
#‘background-size’: ‘150px 100px’
#},
#app = dash.Dash(__name__, update_title=None)

colors = {
    'background': "#000000",
    'text': "#FFFFFF",
    'led': "#93cbf0",
}

# *************************************************************************
app.layout = html.Div(style={'backgroundColor': colors['background']},
    id="dark-light-theme",
    children=[
        html.Div(
            [
                html.H1("Southplains Brewery", style={"textAlign": "center",'color': colors['text']}),
                #html.H2("Water Tank Control", style={"textAlign": "left",'color': colors['text']}),
                html.Br(),
  
                html.Div(

                	html.Table(
  
                   # [html.Tr(
                   #     [

                   #     ])
                   #     ] #+
                   # +

			       # [html.Tr(
			       # 	[

        				#html.Td(
			        	#	daq.Indicator(
                        #	id="Heater-Status",size=100, color=colors['led'],
                        #	label={'label': 'Heater Status','style': {'color': colors['text'],'fontSize':40}},
                    	#	),  
			        	#) #for
        			#	])
                    #    ] #+
			        #+
			        [html.Tr(
			        	[
			        	html.Td(
			    #    		daq.Gauge(
                #        	#style={'color':colors['text']},
                #        	#color={"gradient":True,"ranges":{"blue":[-10,0],"green":[0,60],"yellow":[60,80],"red":[80,100]}},
                #            color={"gradient":True,"ranges":{"green":[0,60],"yellow":[60,80],"red":[80,100]}},
                #        	size=400,                        	     
                #        	showCurrentValue=True,    
                #        	#units="°C",             
                #        	id="Temperature-gauge",
                #        	label={'label': 'Tank 1 ','style': {'color': colors['text'],'fontSize':40}},
                #        	scale={'labelInterval':1, 'style': {'color': colors['text'],'fontSize':80}},
                #        	#custom={'style': {'color': colors['text'],'fontSize':80}},
                #        	min=0, max=100 ,
                #        	style={"textAlign": "left",'color': colors['text'],'margin-top':40,'height':'80%'},
                #    		),
			        	), 
                        html.Td(
                            daq.Gauge(
                            #style={'color':colors['text']},
                            color={"gradient":True,"ranges":{"green":[0,60],"yellow":[60,80],"red":[80,100]}},
                            #color={"gradient":True,"ranges":{"blue":[-10,0],"green":[0,60],"yellow":[60,80],"red":[80,100]}},
                            size=400,                                
                            showCurrentValue=True,    
                            #units="°C",             
                            id="Temperature-gauge2",
                            label={'label': 'Tank 2','style': {'color': colors['text'],'fontSize':40}},
                            scale={'labelInterval':1, 'style': {'color': colors['text'],'fontSize':80}},
                            #custom={'style': {'color': colors['text'],'fontSize':80}},
                            min=0, max=100 ,
                            style={"textAlign": "left",'color': colors['text'],'margin-top':40,'height':'80%'},
                           ),
                        ),
                #        html.Td(
                #            daq.Gauge(
                #            #style={'color':colors['text']},
                #            #color={"gradient":True,"ranges":{"blue":[-10,0],"green":[0,60],"yellow":[60,80],"red":[80,100]}},
                #            color={"gradient":True,"ranges":{"green":[0,60],"yellow":[60,80],"red":[80,100]}},
                #            size=400,                                
                #            showCurrentValue=True,    
                #            #units="°C",             
                #            id="Temperature-gauge4",
                #            label={'label': 'Tank 4','style': {'color': colors['text'],'fontSize':40}},
                #            scale={'labelInterval':1, 'style': {'color': colors['text'],'fontSize':80}},
                #            #custom={'style': {'color': colors['text'],'fontSize':80}},
                #            min=0, max=100 ,
                #            style={"textAlign": "left",'color': colors['text'],'margin-top':40,'height':'80%'},
                #           ),

                #        ),
                        html.Td(
                            daq.NumericInput(
                            id='Tank2-SET',
                            #label='TMP SETPOINT',
                            label={'label': 'Tmp Setpoint','style': {'color': colors['text'],'fontSize':20}},
                            min=0,
                            max=100,
                            size=100,
                            style={'witdh':100,'height':100}
                            ),

                        #html.Td(id='numeric-input-output-1'),
                            
                        #    daq.Knob(
                        #        id='my-knob-1',
                        #        size=300,
                        #        max=100,
                        #        scale={'start':0, 'labelInterval': 5, 'interval': 1}
                        #        ),
                        #        html.Td(id='knob-output-1'),
                            


                        ), 
                        html.Td(
                            #daq.Indicator(
                            #id="Tank2-SHOW",size=40, color=colors['led'],
                            #label={'label': 'Tank2-SHOW','style': {'color': colors['text'],'fontSize':40}},
                            #)
                            daq.ToggleSwitch(
                            id="Tank-TSS", 
                            label={'label': 'Tmp Set [Off / On]','style': {'color': colors['text'],'fontSize':20}},
                            size="100",#value=True
                            ), 
                        ),
                        ])
                        ] #+
                    +
                    [html.Tr(
                        [
                        html.Td(
                        #    daq.Indicator(
                        #    id="Tank1-HTH",size=100, color=colors['led'],
                        #    label={'label': 'Tank1-HTH','style': {'color': colors['text'],'fontSize':40}},
                        #    )  
                        
                        ),
                        html.Td(
                            daq.Indicator(
                            id="Tank2-HTH",size=100, color=colors['led'],
                            label={'label': 'Tank2-HTH','style': {'color': colors['text'],'fontSize':40}},
                            )  
                        
                        ),
                        #html.Td(
                        #    daq.Indicator(
                        #    id="Tank4-HTH",size=100, color=colors['led'],
                        #    label={'label': 'Tank4-HTH','style': {'color': colors['text'],'fontSize':40}},
                        #    )  
                        #
                        #),
                        html.Td(
                            daq.Indicator(
                            id="HTS",size=100, color=colors['led'],
                            label={'label': 'HTS','style': {'color': colors['text'],'fontSize':40}},
                            )  
                        
                        ),
                       	html.Td(
			        		daq.ToggleSwitch(
                    		id="Tank-HTR", 
                    		label={'label': 'NOT USED','style': {'color': colors['text'],'fontSize':40}},
                    		size="200",#value=True
                			),
			        	), #for col in dataframe.columns

                        ])
                        ], #+

        			),
                ),

                html.Div( 
                #    daq.Knob(id='my-knob-1'),
                #    html.Td(id='knob-output-1')

                ),
                html.Div(
                
                ),
                #number_input = html.Div(
                html.Div(
                #[
                #    #html.P("Type a number outside the range 0-10"),
                #    dbc.Input(type="number", min=0, max=10, step=1, size='lg', className="mb-3",html_size='67',
                #        placeholder="A small, valid NOOO", style={'witdh':20,'height':60}),
                #],
                #id="styled-numeric-input",

                ),
                html.Div(
                #[
                #    daq.NumericInput(id='my-numeric-input-2',value=0),
                #  
                #html.Div(id='numeric-input-output-1')
                #]
                ),

                html.Div(

                ),

            ],
            className="row",
        ),
        #html.Div(
        #    html.Div(
        #        daq.ToggleSwitch(
        #            id="my-toggle-switch", label="OFF | ON", size="100"#value=True
        #        ),
        #        className="three columns",
        #    ),
        #    className="row",
        #),
        #html.Div(
        #    dcc.Graph(id="my-graph", figure={}),
        #    className="row",
        #),
        dcc.Interval(id="timing", interval=500, n_intervals=0),
        html.Br(),
        html.Br(),
        html.Br(),
        html.Br(),
        html.Br(),
      	html.Br(),
      	html.Br(),
        html.Br(),
        html.Br(),
        html.Br(),
        html.Br(),
          
    ],
)


#@app.callback(
#    Output('numeric-input-output-1', 'children'),
#    Input('my-numeric-input-2', 'value')
#)
#def update_output(value):
#    return 'The value is {}.'.format(value)


#@app.callback(
#    Output('numeric-input-output-1', 'children'),
#    Input('my-numeric-input-2', 'value')
#)
#def update_output(value):
#    return 'The value is {}.'.format(value)


@app.callback(
    Output('Tank2-SET', 'size'),
    Input('Tank2-SET', 'value')
)
def update_output(value):
    epics.caput('BREWERY:Tank2:TMS',value)
    size =100
    return size


@app.callback(
    #Output("my-tank", "units"),
    Output("Tank-TSS","color"),
    Input("Tank-TSS", "value"),

)
def update_g(toggle):
    if toggle:
        #epics.caput('BWY:Heater:CMD',1)
        epics.caput('BREWERY:Tank2:TSS',1)
        color = "#93cbf0"
       #return "On"
        return color
    else:
        epics.caput('BREWERY:Tank2:TSS',0)
        color = False
        return color





@app.callback(
    #Output("my-tank", "units"),
    Output("Tank-HTR","color"),
    Input("Tank-HTR", "value"),

)
def update_g(toggle):
    if toggle:
        #epics.caput('BWY:Heater:CMD',1)
        epics.caput('BREWERY:Tank2:HTR',1)
        color = "#93cbf0"
       #return "On"
        return color
    else:
        epics.caput('BREWERY:Tank2:HTR',0)
        color = False
        return color


  





# *************************************************************************
# must have Dash 1.16.0 or higher for this to work
@app.callback(
#    Output("Temperature-gauge", "value"),
    Output("Temperature-gauge2", "value"),
    
#    Output("Temperature-gauge4", "value"),

    
    
#    Output("Tank1-HTH","value"),
#    Output("Tank1-HTH","color"),
    Output("Tank2-HTH","value"),
    Output("Tank2-HTH","color"),
#    Output("Tank4-HTH","value"),
#    Output("Tank4-HTH","color"),
    
    Output("HTS","value"),
    Output("HTS","color"),
    

    #Output("my-graph", "figure"),
    Input("timing", "n_intervals"),
)
def update_g(n_intervals):
    

#    Temp_201 = epics.caget('BREWERY:Tank2:TMP.VAL')  # mimics data pulled from live database
    Temp_202 = epics.caget('BREWERY:Tank2:TMP.VAL')
    
#    Temp_204 = epics.caget('BREWERY:Tank2:TMP.VAL')
    


#    Tank1_HTH = epics.caget('BREWERY:Tank2:HTH')
    Tank2_HTH = epics.caget('BREWERY:Tank2:HTH')
#    Tank4_HTH = epics.caget('BREWERY:Tank2:HTH')

    HTS = epics.caget('BREWERY:Tank2:HTS')


#    if (Tank1_HTH):
#        Tank1_HTH_color = "#35BA03"
#    else:
#        Tank1_HTH_color = "#BA0303"    

    if (Tank2_HTH):
        Tank2_HTH_color = "#35BA03"
    else:
        Tank2_HTH_color = "#BA0303"

#    if (Tank4_HTH):
#        Tank4_HTH_color = "#35BA03"
#    else:
#        Tank4_HTH_color = "#BA0303"
    
    if (HTS):
        HTS_color = "#93cbf0"
    else:
        #HTS_color = "#E500EE"
        HTS_color = "#D9DBE5"
    

#    return Temp_201, Temp_202, Temp_204,Tank1_HTH,Tank1_HTH_color,Tank2_HTH,Tank2_HTH_color,Tank4_HTH,Tank4_HTH_color,HTS,HTS_color
#    return Temp_201, Temp_202, Tank1_HTH,Tank1_HTH_color,Tank2_HTH,Tank2_HTH_color,Tank4_HTH,Tank4_HTH_color,HTS,HTS_color
    #return Temp_202, Tank1_HTH,Tank1_HTH_color,Tank2_HTH,Tank2_HTH_color,Tank4_HTH,Tank4_HTH_color,HTS,HTS_color 
    #return Temp_202, Tank2_HTH,Tank2_HTH_color,Tank4_HTH,Tank4_HTH_color,HTS,HTS_color 
    return Temp_202, Tank2_HTH,Tank2_HTH_color,HTS,HTS_color 




#@app.callback(
#)
#def update_output(value):
#    return 'The value is {}.'.format(value)



#@app.callback(Output('knob-output-1', 'children'), Input('my-knob-1', 'value'))
#def update_output(value):
#    return 'The knob value is {}.'.format(value)




  

if __name__ == "__main__":
    #app.run_server(debug=True, port=3030)
    app.run_server(debug=True,host="0.0.0.0", port=3030)