

/*
   Copyright (c) 2018, circuits4you.com
   All rightOs reserved.
   Create a TCP Server on ESP8266 NodeMCU.
   TCP Socket Server Send Receive Demo
*/

#include <ESP8266WiFi.h>
#include <Epics_udp.h>

#define SendKey 0  //Button to send data Flash BTN on NodeMCU

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////// Temperature Sensor ///////////////////////////////////////////////////////////
// Sensor
#include <DallasTemperature.h>
#include <OneWire.h>


int default_port = 8888;  //Port number
WiFiServer server(default_port);


// TESTING VAR
int count = 0;
int temp = 0;
int num = 0;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// EPICS TCP TX
int i_index = 0;
bool b_fetch;
bool b_tx_completed;
char c_mybuffer[6];
char result[4] = {0};




/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////  Constants/////////////////////////////////////////////////////////////////
// Set WiFi credentials
#define WIFI_SSID_Home "COMHEM_a08627"
#define WIFI_PASS_Home "mmzmjdnd"
#define WIFI_SSID_Brewery "Funkyland 2.5ghz"
#define WIFI_PASS_Brewery "Mr Brown"
#define WIFI_SSID_Nacho "Telia-739C30"
#define WIFI_PASS_Nacho "681AE64353"
#define WIFI_SSID_Pau "Telia-615DFB"
#define WIFI_PASS_Pau "D2DCBF3DE2"
#define WIFI_SSID_Temp1 "iPhoneESS"
#define WIFI_PASS_Temp1 "P10204251e"
#define WIFI_SSID_Mamuts "MOVISTAR_AA"
#define WIFI_PASS_Mamuts "938332193"
#define WIFI_SSID_Marta "MOVISTAR_C4E9"
#define WIFI_PASS_Marta "4GRBidha4xip7NyQGxSt"

// Declaration of WIFI
char *WIFI_SSID_Target[] = {WIFI_SSID_Home, WIFI_SSID_Mamuts, WIFI_SSID_Brewery, "END"};
char *WIFI_PASS_Target[] = {WIFI_PASS_Home, WIFI_PASS_Mamuts, WIFI_PASS_Brewery, "END"};

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////// ESP 8266 CONFIGURATION  /////////////////////////////////////////////////////////////////
//const String ESP8266_21_MAC = "F4:CF:A2:E4:A8:7F";
const String ESP8266_21_MAC = "E8:DB:84:C5:8C:B2";

//const String ESP8266_202_MAC = "BC:DD:C2:30:8E:33";
const String ESP8266_22_MAC = "30:83:98:A2:C6:89";
const String ESP8266_23_MAC = "F4:CF:A2:E4:81:00";
const String ESP8266_24_MAC = "30:83:98:B5:53:F5";
const String ESP8266_31_MAC = "C8:C9:A3:0C:03:4A";

String WIFI_MACs[] = {ESP8266_21_MAC, ESP8266_22_MAC, ESP8266_23_MAC, ESP8266_24_MAC, ESP8266_31_MAC, "END"};


const IPAddress ESP8266_21_IP(192, 168, 0, 21);
const IPAddress ESP8266_22_IP(192, 168, 1, 22);
const IPAddress ESP8266_23_IP(192, 168, 1, 23);
const IPAddress ESP8266_24_IP(192, 168, 0, 24);
const IPAddress ESP8266_31_IP(192, 168, 0, 31);
IPAddress WIFI_IPs[] = {ESP8266_21_IP, ESP8266_22_IP, ESP8266_23_IP, ESP8266_24_IP, ESP8266_31_IP};

// Set your Gateway IP address
//IPAddress gateway(192, 168, 1, 1);
IPAddress gateway(192, 168, 0, 1);
IPAddress subnet(255, 255, 255, 0);

const int tcp_iUDPPort_21 = 8021;
const int tcp_iUDPPort_22 = 8022;
const int tcp_iUDPPort_23 = 8023;
const int tcp_iUDPPort_24 = 8024;
const int tcp_iUDPPort_31 = 8031;
int WIFI_TCP_Ports[] = {tcp_iUDPPort_21, tcp_iUDPPort_22, tcp_iUDPPort_23, tcp_iUDPPort_24, tcp_iUDPPort_31};
int WIFI_TCP_Port_Target;


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// What is that doing ?!!? !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// ENABLE PIN
Epics_udp epics_udp(13); // Change the Name of this function

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///// CONSTANTS
const   int HTS_PIN = 4;
const   int HTR_PIN = 5;
const   int TMP_PIN = 12;


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
WiFiClient tcp_client;
String tx_sRequestsType[]  = {"*TMP?", "*HTR?", "*HTR!", "*TMS!", "*TSS!", "*UPL!", "END"};
String hwi_sFieldStatus[] = {"0", "0", "0", "0", "0", "0", "Null"}; // Initialization


///////////////////////////////////////////////////////////////////////////////////////////////////////////
// Temperature Sensor Variables
int   pin_number_sensor = 12;

OneWire oneWirePin(pin_number_sensor);
DallasTemperature sensor(&oneWirePin);

///////////////////////////////// Time Variables /////////////////////////////////////////////////////////////
// Time variables
const unsigned long period = 10000;  //the value is a number of milliseconds
unsigned long startMillis;  //some global variables available anywhere in the program
unsigned long currentMillis;
int elapsed_time;


//=======================================================================
//                    Power on setup
//=======================================================================
void setup()
{



  //////////////////////  Set-up Input/Outputs //////////////


  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(HTR_PIN, OUTPUT);
  //pinMode(TMP_PIN, INPUT);
  //pinMode(HTS_PIN, INPUT);

  ////////////////////// Serial /////////////////////////
  Serial.begin(115200);
  pinMode(SendKey, INPUT_PULLUP); //Btn to send data
  Serial.println();
  int test;
  //WiFiServer server(8881);

  WIFI_TCP_Port_Target = epics_udp.Wifi_Conf_TCP(WIFI_MACs, WIFI_IPs, WIFI_TCP_Ports, gateway, subnet);
  Wifi_Conn_TCP(WIFI_SSID_Target, WIFI_PASS_Target);
  Wifi_ON_Server_TCP(WIFI_TCP_Port_Target);


}

//// Var Time Elapsed
//uint16_t time_elapsed=0;
uint16_t time_elapsed_original;
uint16_t time_elapsed_original_rest;
uint16_t time_elapsed_current;



//=======================================================================================================================
//                    Loop
//=======================================================================================================================

void loop()

{
  //WiFiClient tcp_client = server.available();

  //WiFiClient my_client;


  //char mytest;

  String sTCP_data;
  String DataCompare = "*TMP?";
  int hwi_test = 0;
  String shwi_test;
  //bool b_fetch;
  //bool b_tx_completed;
  //String sTCP_reply;
  int iTCP_reply;
  char tx;
  int b_help;
  //int temp=0;

  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////// TCP CONNECTION /////////////////////////////////////////////
  tcp_client = server.available();

  if (tcp_client) {
    if (tcp_client.connected())
    {
      Serial.println("Client Connected");
    }
    while (tcp_client.connected())
    {
      while (tcp_client.available() > 0)
      {
        ////////////////////////////////////////////////////////// EPICS-TCP LAYER ////////////////////////////////////////////////
        // read data from the connected client
        char tx = tcp_client.read();

        Serial.println("--------------------------------------");
        Serial.println("--------------------------------------");

        sTCP_data = Parse_TCP_ADV_data(tx);
        iTCP_reply = Triage_TCP(sTCP_data, tx_sRequestsType, hwi_sFieldStatus);

        Serial.print("This is the reply: ");
        Serial.println(iTCP_reply);


        if (b_tx_completed)
        {
          Conv_Ascii(iTCP_reply);
          Serial.println("Client Write");
          tcp_client.write(result);
        }

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////// TIME CHECK////////////////////////////////////////////

        time_elapsed_original = millis();
        time_elapsed_original_rest = time_elapsed_original - time_elapsed_current;
        Serial.print("TIME ELAPSED :  ");
        Serial.print(time_elapsed_original_rest);
        Serial.println(" ms");

        time_elapsed_current = millis();
        //delay(1000);

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////// PROCES ///////////////////////////////////////////////

        //hwi_test=round(get_tmp()/10);

        //hwi_test=3;
        if (b_tx_completed == 0) {
          b_help = 1;
        }
        if (b_tx_completed == 1 and b_help == 1) {
          hwi_test++;
          b_help = 0;
          // One delay for MESSAGE COMPLETED
          //delay(1500);
          //hwi_test=round(get_tmp()/10);
          round(get_tmp() / 10);

        }
        if (hwi_test > 9) {
          hwi_test = 0;
        }

        //Serial.print(" this is HWI test");
        //Serial.println(hwi_test);
        hwi_sFieldStatus[0] = String(hwi_test);
        //hwi_test=random(2);
        hwi_sFieldStatus[1] = String(random(2));

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////// HAL //////////////////////////////////////////////////


        HAL();


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

      }
    }

    tcp_client.stop();
    Serial.println("Client disconnected");
  }

}

//============================================================================================================================
//============================================================================================================================

void HAL() {
  Serial.print("Hwi_0 :");
  Serial.println(hwi_sFieldStatus[0]);
  //Conv_Ascii(hwi_sFieldStatus[0]);
  //Serial.print("Hwi_Converted :");
  //Conv_Ascii(hwi_sFieldStatus[0].toInt());


  Serial.print("Hwi_1 :");
  Serial.println(hwi_sFieldStatus[1]);
  Serial.print("Hwi_2 :");
  Serial.println(hwi_sFieldStatus[2]);

  digitalWrite(LED_BUILTIN, !hwi_sFieldStatus[2].toInt());
  digitalWrite(HTR_PIN, hwi_sFieldStatus[2].toInt());

  //get_tmp();
}


void Conv_Ascii(int num) {

  result[0] = num + '0';  // convert to ascii and store
  //Serial.print(result);  // prints "345"
  //return result;

}

//void Wifi_Conn_TCP(WIFI_SSID_Target,WIFI_PASS_Target,tcp_iTCPPort_Target);
void Wifi_Conn_TCP(char *WIFI_SSID_Target_Fun[], char *WIFI_PASS_Target_Fun[])
{
  int i_wifi = 0;

  WiFi.mode(WIFI_STA);

  // TEST
  while (WIFI_PASS_Target_Fun[i_wifi] != "END")
  {
    Serial.println("Trying");
    Serial.println(WIFI_SSID_Target_Fun[i_wifi]);
    //delay(1000);

    //WiFi.mode(WIFI_STA);
    //WiFi.begin(my_ssid, my_password); //Connect to wifi
    WiFi.begin(WIFI_SSID_Target_Fun[i_wifi], WIFI_PASS_Target_Fun[i_wifi]); //Connect to wifi

    // Wait for connection
    int iConn_attempt = 0;
    Serial.println("Connecting to Wifi");
    while (WiFi.status() != WL_CONNECTED and iConn_attempt < 5)
    {
      delay(500);
      Serial.print(".");
      delay(500);
      iConn_attempt++;
    }
    if (WiFi.status() == WL_CONNECTED) {
      break;
    }
    i_wifi++;
    Serial.println(".");


  }


  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(WIFI_SSID_Target_Fun[i_wifi]);

  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
  Serial.print("MAC Address:");
  Serial.println(WiFi.macAddress());



}
void Wifi_ON_Server_TCP(int port)
{
  //int port = 8882;  //Port number
  //WiFiServer server(port);
  //server(port);


  server.begin(port);
  Serial.println("TCP Server Started");
  Serial.println("Open Telnet and connect to IP:");


}

String Parse_TCP_ADV_data(char c) {

  String sTCP_data_fun;
  int iSize_query = 4;
  char c_store;
  //int iSize_Buffer=sizeof(c_mybuffer);

  //Serial.print(i_index);
  //Serial.print(" --- ");
  //Serial.println(c);

  c_store = c;

  if (c == '*')
  {
    Serial.println(" * MESSAGE STARTED ");
    b_fetch = true;
  }

  // if the last char is ! then "te doy otra ronda"
  if ((i_index <= iSize_query or c_mybuffer[i_index - 1] == '!') and b_fetch == true)
    //if(strlen(c_store)>0 and b_fetch==true)
  {
    c_mybuffer[i_index] = c;
    //Serial.println(String(c_mybuffer));
    i_index++;
    //Serial.println(index);
    //mybuffer[index] = '\0';
    b_tx_completed = false;
  }

  // if the last char is ! then "te doy otra ronda"
  if (i_index > iSize_query and b_fetch == true and c_mybuffer[i_index - 1] != '!') // We must be sure we still lyssen the  COMMAND '!'
    //if(strlen(c_store)==0 and b_fetch==true)
  {
    sTCP_data_fun = String(c_mybuffer);
    c_mybuffer[i_index] = '\0';
    //Serial.print("This is the buffer ");
    //Serial.println(String(c_mybuffer));
    Serial.print("This is what i receive : ");
    Serial.println(sTCP_data_fun);
    //Serial.print("Size of the buffer : ");
    //Serial.println(sizeof(c_mybuffer));

    // Empty buffer
    c_mybuffer[0] = '\0';
    c_mybuffer[1] = '\0';
    c_mybuffer[2] = '\0';
    c_mybuffer[3] = '\0';
    c_mybuffer[4] = '\0';


    i_index = 0;
    b_fetch = false;
    b_tx_completed = true;
    Serial.println(" MESSAGE COMPLETED ");
  }


  return sTCP_data_fun;
}




int Triage_TCP(String sFunData_received, String udp_sFunRequestsType[], String hwi_sFunFieldStatus[]) {

  String sFunReply;
  int iFunReply;
  int iPos_admir;
  int iPacketSize;
  String sVal;
  String sData;

  int i = 0;
  while (udp_sFunRequestsType[i] != "END") {


    // REQUEST of DATA : sFunData_received contains ? --
    if ((sFunData_received.indexOf("?") != -1) and sFunData_received == udp_sFunRequestsType[i]) { // Data Request CAGET  // 0. *TMP? // 1. *HTR? // etc
      sFunReply = hwi_sFunFieldStatus[i];
      iFunReply = sFunReply.toInt();
      goto end_triage;
    }


    // COMMAND of Data : It contains ! and Matches One of the Commands
    if ((sFunData_received.indexOf("!") != -1) and (sFunData_received.indexOf(udp_sFunRequestsType[i])) != -1) { // Data Assignment CAPUT // 2. *HTR! // etc

      iPacketSize = sFunData_received.length();
      iPos_admir = sFunData_received.indexOf('!');
      sVal = sFunData_received.substring(iPos_admir + 1, iPacketSize);

      hwi_sFunFieldStatus[i] = sVal;
      //hwi_sFunFieldStatus[i] = "3";
      

      //sFunReply="1";
      //iFunReply = 1;
      //iFunReply = hwi_sFunFieldStatus[i].toInt();
      iFunReply = sVal.toInt();

      goto end_triage;
    }
    else {
      iFunReply = 0;
      i = i + 1;
    }
  }

end_triage:
  return iFunReply;

}

////////////////////////////////////////////////////////////////////////////////////////////////////////7
////////////////////////////////////////////////////////////////////////////////////////////////////////7

int get_tmp() {
  int tmp;

  sensor.requestTemperatures();
  tmp = sensor.getTempCByIndex(0);
  //tmp = random(100);
  Serial.print(" Temperature :");
  Serial.println(tmp);

  return tmp;

}
