// Sensor 
#include <DallasTemperature.h>
#include <OneWire.h>


// Hardware Access Layer : WiFi
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>


// Set your access point network credentials  --> Change the SSID and Pass to HWA
const char* ssid = "ESP8266-Access-Point";
const char* password = "123456789";

// Data
//int sensorVal;
//float sensor_value=0.0;
//int binari=0;

// Sensor
int   pin_number_sensor =5;
float f_temperature = 0;      //Variable to store the temperature in

OneWire oneWirePin(pin_number_sensor);
DallasTemperature sensors(&oneWirePin);



int i_temperature;
int i_heater_CMD;
int i_heater_RB;
int i_pressure;


ESP8266WebServer Tank_server;





void setup(){
  
///////////////////  Start UP the INTERFACES ///////////////////////////////////////////
///////////////////  Serial PORT //////////////
// Serial port for debugging purposes
  //Serial.begin(115200);
  Serial.begin(9600);
  //Serial.println();
  
///////////////////  HWA : Wifi //////////////
// Setting the ESP as an access point
  Serial.print("Setting AP (Access Point)…");
// Remove the password parameter, if you want the AP (Access Point) to be open
  WiFi.softAP(ssid, password);

  IPAddress IP = WiFi.softAPIP();
  Serial.print("AP IP address: ");
  Serial.println(IP);
  
///////////////////  Set-up Outputs //////////////
pinMode(LED_BUILTIN, OUTPUT);

 

///////////////////  HWA : Define and Start HTTP Variables //////////////

  //Tank_server.on("/",handleIndex); // use the top root path to report the last sensor value
  //Tank_server.on("/update",handleUpdate);
  //Tank_server.on("/temperature",handleTemp);
  
  Tank_server.on("/heater_CMD",handle_heater_CMD); 
  Tank_server.on("/heater_CMD_update",handle_heater_CMD_update);
  Tank_server.on("/temperature",handle_temperature);
  Tank_server.on("/pressure",handle_pressure);
  Tank_server.begin();


//////////////////// Sensor DS1820 ///////////////////////////////////
  sensors.begin();
  
}
 
void loop(){

  ///////////////////////// Field ///////////////////////////////////
  sensors.requestTemperatures(); 
  f_temperature = sensors.getTempCByIndex(0);
    
  Serial.print("Temperature is ");
  Serial.println(f_temperature);
  
  //Serial.print("The value of the Sensor:");

  //i_temperature = 444;
  
  // THIS IS NOT USEFUL CAUSE IS NOT CONNECTED TO THE SERIAL DEVICE.
  Serial.println("Temperature :" + String(f_temperature));
  Serial.println("Heater_CMD :" + String(i_heater_CMD));

  
  //Serial.println("Go");

  //PROCESS 
  //if 


///////////////////  PROCESS ///////////////////////////////////////////
  if (i_heater_CMD){
    digitalWrite(LED_BUILTIN, HIGH);
  }
  else{
    digitalWrite(LED_BUILTIN, LOW);
  }
///////////////////////////////////////////////////////////////////////


///////////////////  HWA : Define and Start HTTP Variables //////////////
///////////////////  Handle Clients periodically //////////////
  Tank_server.handleClient();
  delay(400);
  
}


///////////////////  FUNCTIONS ///////////////////////////////////////////


void handle_heater_CMD(){
  Tank_server.send(200,"text/plain",String(i_heater_CMD)); // 
}

void handle_temperature(){
  //i_temperature = analogRead(A0);

  //i_temperature = random(999);
  //Tank_server.send(200,"text/plain",String(i_temperature)); // 
  Tank_server.send(200,"text/plain",String(f_temperature)); // 
}

void handle_pressure(){
  //i_temperature = analogRead(A0);
 
  i_pressure = 333;
  Tank_server.send(200,"text/plain",String(i_pressure)); // 
}



void handle_heater_CMD_update(){
  // Take the value
 
  i_heater_CMD=Tank_server.arg("value").toInt();
  Serial.println(i_heater_CMD);
  // Inform about the update
  Tank_server.send(200,"text/plain","Updated");
}


//  C2S_server.on("/temperature", HTTP_GET, [](AsyncWebServerRequest *request){
//    request->send_P(200, "text/plain", readTemp().c_str());
//  });

//  C2S_server.on("/humidity", HTTP_GET, [](AsyncWebServerRequest *request){
//    request->send_P(200, "text/plain", readHumi().c_str());
//  });

//  C2S_server.on("/pressure", HTTP_GET, [](AsyncWebServerRequest *request){
//    request->send_P(200, "text/plain", readPres().c_str());
//  });

//  C2S_server.on("/test", HTTP_GET, [](AsyncWebServerRequest *request){
//    request->send_P(200, "text/plain", handleUpdate());
//  });
