//////////////////////////////////////////////////////////////// LIBRARIES //////////////////////////////////////

///////////////////////////////// UDP Connection //////////////////////////////////////////////////////////////
//#include <ESP8266WiFi.h>
#include <WiFi.h>             ## FOR ESP32
#include <WifiUdp.h>
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////// Temperature Sensor ///////////////////////////////////////////////////////////
// Sensor 
#include <DallasTemperature.h>
#include <OneWire.h>
////////////////////////////////////////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////CONSTANTS & VARIABLES //////////////////////////////////////

////////////////////////////////// WIFI /////////////////////////////////////////////////////////////////////////
// Set WiFi credentials
#define WIFI_SSID "8==D"
#define WIFI_PASS "jakob_cool"




///////////////////////////////// UDP Layer Variable and Constants///////////////////////////////////////////////

// UDP Port
#define UDP_PORT 4211

// UDP Layer Variables
WiFiUDP UDP;
char udp_cPacket_notused[25];
int udp_iUDPPort = 4211;

// UDP Nomenclature
const String udp_sTank_tmp="TMP?";        // UPD Temperature Request
const String udp_sHeater_rbk="HTR?";       // UPD Heater Readback
const String udp_sHeater_cmd="HTR!";



///////////////////////////////// Hardware Access Layer Variables /////////////////////////////////////////////
// Hardware Access Layer Variables
int hwi_iTank_tmp;                     // Temperature of the tank
bool hwi_bHeater_rbk;                   // Heater Readback
bool hwi_bHeater_cmd;                   // Heater Command

// Temperature Sensor Variables
int   pin_number_sensor =12;
//float f_temperature = 0;             //Variable to store the temperature in

OneWire oneWirePin(pin_number_sensor);
DallasTemperature sensor(&oneWirePin);

///////////////////////////////// Time Variables /////////////////////////////////////////////////////////////
// Time variables
const unsigned long period = 10000;  //the value is a number of milliseconds
unsigned long startMillis;  //some global variables available anywhere in the program
unsigned long currentMillis;


////////////////////////////////////// General Variables  /////////////////////////////////////////////////////

bool LED_STATE;


 
void setup() {

//////////////////////  Set-up Inputs and Outputs //////////////
//  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(12,INPUT);
  //pinMode(5,INPUT);
  //pinMode(6,INPUT);
  //pinMode(7,INPUT);
  //pinMode(8,INPUT);
  

/////////////////////////////////////////////////////
  
  
  // Setup serial port
  Serial.begin(115200);
  Serial.println();
   
  // Begin WiFi
  WiFi.begin(WIFI_SSID, WIFI_PASS);
   
  // Connecting to WiFi...
  Serial.print("Connecting to ");
  Serial.print(WIFI_SSID);
  // Loop continuously while WiFi is not connected
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(1000);
    Serial.print(".");
  }
   
  // Connected to WiFi
  Serial.println();
  Serial.print("Connected! IP address: ");
  Serial.println(WiFi.localIP());

// Begin listening the UDP port
UDP.begin(UDP_PORT);
Serial.print("Listening on UDP port ");
Serial.println(UDP_PORT);
  

  
}
   
void loop() {
 
  
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////// TEST ///////////////////////////////////////////
  //blinking_led();


  ////////////////////////////// UDP LAYER ////////////////////////////////////////////
  // UDP Layer
  // IMPROVEMENT of this FUNCTION is neeeded !!!!!! Create a Array of INPUTS and treat the RETURN of the Function
  //parse_udp_data(udp_sTank_tmp,hwi_iTank_tmp); 
  //parse_udp_data(udp_sHeater_rbk,hwi_bHeater_rbk); 
  parse_udp_data();

   //////////////////////////// PROCESS TEST ///////////////////////////////////////////
      // TESSSSSSSSTTTTTTTTTTTTT 
    // MOVING DATAAAAAAAAAAAA
    hwi_iTank_tmp=get_tmp();
//    hwi_bHeater_rbk=!digitalRead(LED_BUILTIN);
   /////////////////////////////////////////////////////////////////////////////////////
  


  /////////////////////////////// HARDWARE ACCESS LAYER ////////////////////////////////
//  if (hwi_bHeater_cmd==0){digitalWrite(LED_BUILTIN,HIGH);}
//  if (hwi_bHeater_cmd==1){digitalWrite(LED_BUILTIN,LOW);}

// TEST without LOAD
if (hwi_bHeater_cmd==0){hwi_bHeater_rbk=0;}
if (hwi_bHeater_cmd==1){hwi_bHeater_rbk=1;}




  

   
    
}





//////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////// FUNCTIONS /////////////////////////////////

//int parse_udp_data(String udp_var,int hwi_var){
void parse_udp_data(){

  /////////////////////////////////////////////////////////
  // Definition of Variables 
  char cPacket[25];
  char cReply[25];
  String sPacket;
  String sReply;              // ESP8266
  int iReply;                 // ESP32
  
  String sVal;
  String sData;
  
  
  int iPos_type;
  int iPos_star;
  int iPos_query;
  int iPos_admir;
  int iPos_equal;

 
//////////////////////////////////////////////FUNCTIONS ///////////////////////////////////////////////////////////
  // If packet received...
  int iPacketSize = UDP.parsePacket();
  
  //Serial.print("First Hello");
  //Serial.println(packetSize);

  if (iPacketSize) {
    Serial.print("Received packet! Size: ");
    Serial.println(iPacketSize);
    int len = UDP.read(cPacket, 255);
    if (len > 0)
    {
      cPacket[len] = '\0';
    }
    Serial.print("Packet received: ");
    Serial.println(cPacket);


    // GET DATA ////////////////////////////////////////////
    sPacket=String(cPacket);
    Serial.println(sPacket);

    iPos_star = sPacket.indexOf('*');
    iPos_query = sPacket.indexOf('?');
    iPos_admir = sPacket.indexOf('!');
    iPos_equal=  sPacket.indexOf('=');
    if (iPos_query>0){
      sData = sPacket.substring(iPos_star+1,iPacketSize);
    }
    else{
      sData = sPacket.substring(iPos_star+1,iPos_equal);
    }
    
    // GREEN = LIGHT --> If we have STAR
    ////////////////////////////////////////////////////////

   
    // TRIAGE
    Serial.print("DATA:");
    Serial.println(sData);
    
    if (sData==udp_sTank_tmp){  
      sReply=String("*"+udp_sTank_tmp+"="+hwi_iTank_tmp);         // ESP8266
      //sReply=String("*"+udp_var+"="+hwi_var);
      iReply=hwi_iTank_tmp;                                     // ESP32
      goto end_triage;
    }    
    if (sData==udp_sHeater_rbk){  
      sReply=String("*"+udp_sHeater_rbk+"="+hwi_bHeater_rbk);     // ESP8266
      iReply=hwi_bHeater_rbk;                                     // ESP32
      goto end_triage;
    }
    if (sData==udp_sHeater_cmd){  
      sVal = sPacket.substring(iPos_equal+1,iPacketSize);
      hwi_bHeater_cmd=sVal.toInt();
      //Serial.println(sVal);
      sReply=String("*"+udp_sHeater_cmd+"="+"OK");                // ESP8266
      iReply=1;                                                   // ESP32
      
      goto end_triage;
    }
    else{
      sReply=String(-1);                                          // ESP8266
      iReply=0;                                                   // ESP32
    }

    end_triage:
    
    
    sReply =String(iReply);                                 // THIS IS ESP32
    //sReply.toCharArray(cReply,25);                        // THIS IS ESP8266

    
    unsigned char ccReply[2];                              // THIS IS ESP32
    ccReply[0]=sReply[0];                                  // THIS IS ESP32
    ccReply[1]=sReply[1];                                  // THIS IS ESP32
    
      
    UDP.beginPacket(UDP.remoteIP(), udp_iUDPPort);
    //UDP.beginPacket(UDP.remoteIP(), 4211);
   
    Serial.println(UDP.remoteIP());
    Serial.println(UDP.remotePort());
    Serial.println("ESP8266 REPLY : "+sReply);
    
    Serial.println("ESP32 sREPLY: "+sReply);
    Serial.println("ESP32 ccREPLY 0: "+String(ccReply[0]));
    Serial.println("ESP32 ccREPLY 1: "+String(ccReply[1]));
    
    //UDP.write(cReply);                                   // THIS IS ESP8266
    
    UDP.write(ccReply[0]);                                 // THIS IS ESP32
    UDP.write(ccReply[1]);                                 // THIS IS ESP32
    
    UDP.endPacket();
  }
  
  // To implement in the future
  //return 1;
}

//void blinking_led(){
//
//   currentMillis = millis();  //get the current "time" (actually the number of milliseconds since the program started)
//
//  if (currentMillis - startMillis >= period) { //test whether the period has elapsed
//    
//    if (LED_STATE==1){
///       digitalWrite(LED_BUILTIN,LOW);
//       LED_STATE=0;
//       Serial.println("false");
//    }else{
///       digitalWrite(LED_BUILTIN,HIGH);
//     LED_STATE=1;
//       Serial.println("true");
//    }
//    Serial.println("TMP:");
//    Serial.println(hwi_iTank_tmp);
//    startMillis = currentMillis;  //IMPORTANT to save the start time of the current LED state.
//  }
//}

int get_tmp(){
  int tmp;
  
  sensor.requestTemperatures(); 
  //tmp=sensor.getTempCByIndex(0);
  tmp=random(100);

  return tmp;

}
  
