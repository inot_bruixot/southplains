

//////////////////////////////////////////////////////////////// LIBRARIES //////////////////////////////////////

///////////////////////////////// UDP Connection //////////////////////////////////////////////////////////////
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////// Temperature Sensor ///////////////////////////////////////////////////////////
// Sensor
#include <DallasTemperature.h>
#include <OneWire.h>
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////Arduino OTA //////////////////////////////////////////////////////////////////
#include <ArduinoOTA.h>
//#include <Morse.h>
#include <Epics_udp.h>

//////////////////////////////////////////////////////////////// VARIABLES /////////////////////////////////////

///////////////////////////////// UDP Layer Variable and Constants///////////////////////////////////////////////
// Set WiFi credentials
#define WIFI_SSID_Home "8==D"
#define WIFI_PASS_Home "jakob_cool"
#define WIFI_SSID_Brewery "Funkyland 2.5ghz"
#define WIFI_PASS_Brewery "Mr Brown"
#define WIFI_SSID_Nacho "Telia-739C30"
#define WIFI_PASS_Nacho "681AE64353"
#define WIFI_SSID_Rocket "DWR-953-A2A6"
#define WIFI_PASS_Rocket "R4DGLXGQ"
#define WIFI_SSID_Temp1 "iPhoneESS"
#define WIFI_PASS_Temp1 "P10204251e"
#define WIFI_SSID_Mamuts "MOVISTAR_AA"
#define WIFI_PASS_Mamuts "938332193"



#define UDP_PORT 4210

// UDP Layer Variables
WiFiUDP UDP_old;

int udp_iUDPPort_Target;

// UDP Nomenclature --> NOT USED CURRENTLY
//char *udp_sRequest[]  = {"*TMP?", "*HTR?", "*HTR!","*UPL!","END"};
//char *hwi_sField[]= {"0","0","0","0","Null"};

// UDP Nomenclature
//String Triage_Update(String sFunData_received,String udp_sFunRequestsType[],String hwi_sFunFieldStatus[]){
String udp_sRequestsType[]  = {"*TMP?", "*HTR?", "*HTR!","*UPL!","END"};
String hwi_sFieldStatus[] = {"0","0","0","0","Null"};

//const String udp_sTank_tmp = "TMP?";      // UPD Temperature Request
//const String udp_sHeater_rbk = "HTR?";     // UPD Heater Readback
//const String udp_sHeater_cmd = "HTR!";

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////// ESP 8266 CONFIGURATION  /////////////////////////////////////////////////////////////////
const String ESP8266_21_MAC = "E8:DB:84:C5:8C:B2";
//const String ESP8266_202_MAC = "BC:DD:C2:30:8E:33";
const String ESP8266_23_MAC = "30:83:98:A2:C6:89";
const String ESP8266_24_MAC = "F4:CF:A2:E4:81:00";
const String ESP8266_22_MAC = "F4:CF:A2:E4:A8:7F";

String WIFI_MACs[]={ESP8266_21_MAC,ESP8266_22_MAC,ESP8266_23_MAC,ESP8266_24_MAC,"END"};
const IPAddress ESP8266_21_IP(192, 168, 88, 21);
const IPAddress ESP8266_23_IP(192, 168, 0, 23);
const IPAddress ESP8266_24_IP(192, 168, 88, 24);
const IPAddress ESP8266_22_IP(192, 168, 88, 22);
IPAddress WIFI_IPs[]={ESP8266_21_IP,ESP8266_22_IP,ESP8266_23_IP,ESP8266_24_IP};
const int udp_iUDPPort_21 = 4021;
const int udp_iUDPPort_22 = 4022;
const int udp_iUDPPort_23 = 4023;
const int udp_iUDPPort_24 = 4024;

int WIFI_UDPs[]={udp_iUDPPort_21,udp_iUDPPort_22,udp_iUDPPort_23,udp_iUDPPort_24};

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  

///////////////////////////////// Hardware Access Layer Variables /////////////////////////////////////////////
// Hardware Access Layer Variables
int hwi_iTank_tmp;                     // Temperature of the tank
bool hwi_bHeater_rbk;                   // Heater Readback
bool hwi_bHeater_cmd;                   // Heater Command

// Temperature Sensor Variables
int   pin_number_sensor = 12;

OneWire oneWirePin(pin_number_sensor);
DallasTemperature sensor(&oneWirePin);

///////////////////////////////// Time Variables /////////////////////////////////////////////////////////////
// Time variables
const unsigned long period = 10000;  //the value is a number of milliseconds
unsigned long startMillis;  //some global variables available anywhere in the program
unsigned long currentMillis;
int elapsed_time;


////////////////////////////////////// General Variables  /////////////////////////////////////////////////////
bool LED_STATE;
const   int HTR_PIN = 5;
const   int TMP_PIN = 12;

/////////////////////////////////////////// IP Variables //////////////////////////////////////////////////////
// Definition of the IP
// Set your Static IP address

// Set your Gateway IP address
IPAddress gateway(192, 168, 0, 1);
IPAddress subnet(255, 255, 255, 0);
IPAddress Blue_IP(192, 168, 1, 211);

//Morse morse(13);
Epics_udp epics_udp(13);

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////// SETUP /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


void setup() {


  //////////////////////  Set-up Outputs //////////////


  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(HTR_PIN, OUTPUT);
  pinMode(TMP_PIN, INPUT);


  /////////////////////////////////////////////////////

  // Setup serial port
  Serial.begin(115200);
  Serial.println();

  /////////////////////////////////////////////////////

  char *proc_sStatus[]  = {"*TMP?", "*HTR?", "*HTR!","END"};  
  
  IPAddress Target_IP;

  
  
udp_iUDPPort_Target=epics_udp.Wifi_Conf(WIFI_MACs,WIFI_IPs,WIFI_UDPs, gateway,subnet);

//Serial.println("PORT UDP");
//Serial.println(udp_iUDPPort_Target);
  
  
  
//////////////////////////////////////////// WIFI Connection ///////////////////////////////////////////////////////////////////////////////////////////////

// Declaration of WIFI
char *WIFI_SSID_Target[]={WIFI_SSID_Home, WIFI_SSID_Brewery, WIFI_SSID_Rocket,WIFI_SSID_Brewery, "END", "This is string 5", "This is string 6"};
char *WIFI_PASS_Target[]={WIFI_PASS_Home, WIFI_PASS_Brewery, WIFI_PASS_Rocket,WIFI_PASS_Brewery, "END", "This is string 5", "This is string 6"};
  
epics_udp.Wifi_Conn(WIFI_SSID_Target,WIFI_PASS_Target);




//////////////////////////////////////////// END WIFI Connection ///////////////////////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////// OTA PROCESS ///////////////////////////////////////////////////////////////////////////////////////////

  ArduinoOTA.onStart([]() {
    String type;
    if (ArduinoOTA.getCommand() == U_FLASH) {
      type = "sketch";
    } else { // U_FS
      type = "filesystem";
    }

    // NOTE: if updating FS this would be the place to unmount FS using FS.end()
    Serial.println("Start updating " + type);
  });
  ArduinoOTA.onEnd([]() {
    Serial.println("\nEnd");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) {
      Serial.println("Auth Failed");
    } else if (error == OTA_BEGIN_ERROR) {
      Serial.println("Begin Failed");
    } else if (error == OTA_CONNECT_ERROR) {
      Serial.println("Connect Failed");
    } else if (error == OTA_RECEIVE_ERROR) {
      Serial.println("Receive Failed");
    } else if (error == OTA_END_ERROR) {
      Serial.println("End Failed");
    }
  });
  ArduinoOTA.begin();
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////// UDP BEGIN LISTEN //////////////////////////////////////////////////////////////////////////////////////////////////////////

// Begin listening the UDP port
UDP_old.begin(UDP_PORT);
Serial.print("Listening on UDP port ");
Serial.println(UDP_PORT);

//////////////////////////////////////////// UDP END //////////////////////////////////////////////////////////////////////////////////////////////////////////







}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////// END SETUP/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////




bool ota_flag=true;
//bool ota_flag;
uint16_t time_elapsed=0;
uint16_t time_elapsed_original;
uint16_t time_elapsed_original_rest;
uint16_t time_elapsed_current;


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////// START LOOP///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



void loop() {


//Serial.print("OTA FLAG UP ");
//Serial.println(ota_flag);
//Serial.print("OTA TIME ELAPSED UP ");
time_elapsed_original=millis();
Serial.print("TIME ELAPSED ORIGINAL ");
Serial.println(time_elapsed_original);

time_elapsed_original_rest=65536-time_elapsed_original;
Serial.print("TIME ELAPSED ORIGINAL RESTA");
Serial.println(time_elapsed_original_rest);

time_elapsed_current=millis();
Serial.print("TIME ELAPSED CURRENT");
Serial.println(time_elapsed_current);

//delay(3000);

//while(time_elapsed<5000){
//  if (time_elapsed_current>time_elapsed_original){
//    time_elapsed = time_elapsed_current-time_elapsed_original;
//    Serial.print("TIME STRAIGHTTT");
//    Serial.println(time_elapsed);
//    time_elapsed_current=millis();
//  }
//  else{
//    time_elapsed = time_elapsed_current+time_elapsed_original_rest;
//    Serial.print("TIME INVERTED");
//    Serial.println(time_elapsed);
//    time_elapsed_current=millis();
//  }
//}
//  time_elapsed=0;   


  
  //////////////////////////// OTA /////////////////////////////////////////////////////
  if(ota_flag){
      //while(time_elapsed<15000){
      //time_elapsed_original=millis();
      //while(time_elapsed<10000){
      while(time_elapsed<5000){
          //Serial.println("Yes we are here");
          //delay(2000);
          if (time_elapsed_current>time_elapsed_original){
              ArduinoOTA.handle();
          //time_elapsed=millis();
              time_elapsed = time_elapsed_current-time_elapsed_original;
              time_elapsed_current=millis();
              Serial.print("OTA STRAIGHT"  );
              Serial.println(time_elapsed);
              delay(30);
              digitalWrite(LED_BUILTIN,!digitalRead(LED_BUILTIN));
          }
          else{
            
              ArduinoOTA.handle();
              time_elapsed = time_elapsed_current+time_elapsed_original_rest;
              time_elapsed_current=millis();
              Serial.print("OTA REVERSE : " );
              Serial.println(time_elapsed);
              delay(30);
              digitalWrite(LED_BUILTIN,!digitalRead(LED_BUILTIN));
          }
      }
      ota_flag=false;               // Reset Flag
      hwi_sFieldStatus[3]=ota_flag;    // Reset Array --> Position [UPL!]
      time_elapsed=0;
            
    }

  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////// TEST ///////////////////////////////////////////
  //blinking_led();



// Assignment of DATA into the ARRAY of DATA, it must be another function IT MUST GO BEFORE because it restets/
// ANALIZE why that happens
// ??????????? What is that 

//char cstr[16];
//hwi_iTank_tmp=random(100);
//String str=String(hwi_iTank_tmp);
//str.toCharArray(cstr,16);
//hwi_sField[0]=cstr;


///////////////////////////////////////////////////////////////////////////////

  
////////////////////////////// UDP LAYER ////////////////////////////////////////////
// UDP Layer
// IMPROVEMENT of this FUNCTION is neeeded !!!!!! Create a Array of INPUTS and treat the RETURN of the Function
//parse_udp_data(udp_sTank_tmp,hwi_iTank_tmp);
//parse_udp_data(udp_sHeater_rbk,hwi_bHeater_rbk);
 
//Serial.print("Before here the HWI_SFIELD [0]");
//Serial.println(hwi_sField[0]);
  
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//// EPICS UDP LAYER
  
String sUdp_data;                                                     // UPD DATA received 
String sReply;                                                        // Reply to be send (string)
char cReply[25];                                                      // Reply to be send (char)

//Serial.println("Ahi esta");
//Serial.println(udp_iUDPPort_Target);
sUdp_data=epics_udp.Parse_udp_data(UDP_old);
if (sUdp_data != "0"){
    //sReply = epics_udp.Triage(udp_sRequest,sUdp_data,hwi_sField);
    sReply = Triage_Update(sUdp_data,udp_sRequestsType,hwi_sFieldStatus);
    epics_udp.Send_udp_data(sReply, UDP_old, udp_iUDPPort_Target);
}

//String udp_sRequestsType[]  = {"*TMP?", "*HTR?", "*HTR!","END"};
//String hwi_sFieldStatus[] = {"0","0","0","Null"};

//Serial.println(udp_sRequestsType[2]);
//Serial.println(hwi_sFieldStatus[2]);

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////// PROCESS TEST ///////////////////////////////////////////
// TESSSSSSSSTTTTTTTTTTTTT
// MOVING DATAAAAAAAAAAAA
hwi_iTank_tmp = get_tmp();


//if(hwi_iTank_tmp > 99){
//  hwi_iTank_tmp=0;
//}
//else{
//  hwi_iTank_tmp=hwi_iTank_tmp+2;
//}





//hwi_bHeater_rbk = !digitalRead(LED_BUILTIN);
//digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
  


// CICLE TIME
currentMillis = millis();  //get the current "time" (actually the number of milliseconds since the program started)
elapsed_time = currentMillis - startMillis;
Serial.println(String(elapsed_time));
startMillis = currentMillis;


/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////// HARDWARE ACCESS LAYER ////////////////////////////////

//morse.BlinkLed(200); 
int itest;
String stest;
hwi_sFieldStatus[0]= String(hwi_iTank_tmp);
stest = hwi_sFieldStatus[2];
itest = stest.toInt();
digitalWrite(LED_BUILTIN,!itest);
stest = hwi_sFieldStatus[3];
itest = stest.toInt();
ota_flag = itest;
Serial.print("OTA FLAG DOWN ");
Serial.println(ota_flag);

}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////// END LOOP///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////// FUNCTIONS ////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int get_tmp() {
  int tmp;

  sensor.requestTemperatures();
  tmp=sensor.getTempCByIndex(0);
  //tmp = random(100);

  return tmp;

}


String Triage_Update(String sFunData_received,String udp_sFunRequestsType[],String hwi_sFunFieldStatus[]){

  String sFunReply;
  int iPos_admir;
  int iPacketSize;
  String sVal;

  int i=0;
  while (udp_sFunRequestsType[i] != "END"){
      // REQUEST of DATA : sFunData_received contains ? --
      if ((sFunData_received.indexOf("?")!=-1) and sFunData_received == udp_sFunRequestsType[i]) {  // Data Request CAGET  // 0. *TMP? // 1. *HTR? // etc
          sFunReply=hwi_sFunFieldStatus[i]; 
          goto end_triage;
      }
      // ASSIGNMENT of DATA : sFunData_received contains ! --
      if ((sFunData_received.indexOf("!")!=-1) and (sFunData_received.indexOf(udp_sFunRequestsType[i])) != -1) {  // Data Assignment CAPUT // 2. *HTR! // etc
          iPacketSize = sFunData_received.length();
          iPos_admir = sFunData_received.indexOf('!');
          sVal = sFunData_received.substring(iPos_admir + 1, iPacketSize);
          hwi_sFunFieldStatus[i]=sVal;
          ////hwi_bHeater_cmd = sVal.toInt();
          ////Serial.println("SVAL:" + sVal);
          //sFunReply=hwi_sFunField[i];
          sFunReply="OK";
          Serial.println(sFunData_received);
          Serial.println(hwi_sFunFieldStatus[i]);
          goto end_triage;
      }
      else {
          sFunReply="DATA_UNKNOWN";
          i=i+1;
      }
  }

  end_triage:
  return sFunReply;

}
