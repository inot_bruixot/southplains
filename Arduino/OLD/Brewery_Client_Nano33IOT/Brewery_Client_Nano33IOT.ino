//#include <OneWire.h>

//#include <TimeLib.h>

//#include <CurieTime.h>
//#include <DateTime.h>
#include <TimeLib.h>

long randNumber;
unsigned long Time;
int test;
//bool Blue=0;
//bool Yellow=0;
//bool YellowState;
int MyHour;
//bool permit;
int python_time;

int tank_server_out=0;

//String fuck;
//int sensorVal;
//const int sensorPin = A2;
//int val;
int hora;
int minu;
bool wakeup;
int target_time;
int target_hour;
int target_min;
int current_time;
int target_tmp;
int hwa_temperature;
int turn;
bool Auto_man;
int i_heartbit;

//////////// Sensor /////////////////////////////////////////////////////
int temp_sensor = 5;       // Pin DS18B20 Sensor is connected to
float temperature = 0;      //Variable to store the temperature in





/////////////////////// EPICS LAYER  /////////////////////////////////////

//unsigned long startMillis;  //some global variables available anywhere in the program
unsigned long T_EpicsSendData_Last;
//unsigned long currentMillis;
unsigned long T_EpicsSendData_Current;
const unsigned long period_EpicsSendData = 500;  //the value is a number of milliseconds

bool heater_CMD;
bool heater_RDB;
String DataBody;

unsigned long T_Heartbit_Last;
unsigned long T_Heartbit_Current;
const unsigned long period_Heartbit = 100;  //the value is a number of milliseconds



////////////////////// HWA LIBRARIES /////////////////////////////////////
#include <SPI.h>
#include <WiFiNINA.h>
#include <ArduinoHttpClient.h>

#include "arduino_secrets.h"
///////////////////// HWA LIBRARIES /////////////////////////////////////

///////////////////////////////////////// HWA : Variables and Constants /////////////////////////////////

char ssid[] = SECRET_SSID;
char pass[] = SECRET_PASS;

//char ssid[] = "ESP8266-Access-Point";
//char pass[] = "123456789";

// Name of the server we want to connect to
//const char kHostname[] = "arduino.cc";
const char kHostname[] = "192.168.4.1";
// Path to download (this is the bit after the hostname in the URL
// that you want to download
const String kPath_tmp = "/temperature";
const String kPath_press = "/pressure";
const String kPath_heater_CMD = "/heater_CMD";


// Number of milliseconds to wait without receiving any data before we give up
//const int kNetworkTimeout = 30*1000;
const int kNetworkTimeout = 30*10;

// Number of milliseconds to wait if no data is available before trying again
//const int kNetworkDelay = 1000;
const int kNetworkDelay = 10;

WiFiClient c;
//HttpClient http(c, kHostname);
HttpClient http(c,"192.168.4.1");

///////////////////////////////////////////////////////////////////////////////////////////////////////////


void setup() {
  Serial.begin(9600);
  Serial.setTimeout(15000UL); // Timeout (15 seconds)
  pinMode(6,OUTPUT);
  pinMode(3,OUTPUT);
  pinMode(2,OUTPUT);
  pinMode(LED_BUILTIN, OUTPUT);
    
  randomSeed(analogRead(0));
  //time();

//////////////////////////////////// HWA Setup Start /////////////////////////////

  
  
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }

  // attempt to connect to Wifi network:
  Serial.println("Attempting to connect to WPA SSID: ");
  Serial.println("-----");
  Serial.println(ssid);
  Serial.println(pass);
  Serial.println("-----");
  
  while (WiFi.begin(ssid, pass) != WL_CONNECTED) {
    // unsuccessful, retry in 4 seconds
    Serial.print("failed ... ");
    delay(4000);
    //delay(40);
    Serial.print("retrying ... ");
    delay(4000);

    tank_server_out=tank_server_out+1;

  if (tank_server_out>3){
    Serial.println();
    Serial.println("NOT connected");
    break;
    
  }
     

    
  }

  Serial.println("connected");

}

/////////////////////////////////////////////////////////////////////////////////////
  


void loop() {


  //digitalWrite(LED_BUILTIN,HIGH);
  //digitalWrite(LED_BUILTIN,HIGH);
  //digitalWrite(LED_BUILTIN,LOW);

  

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////// EPICS SERIAL LAYER  ///////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////


  epics_layer(); 
  
  i_heartbit=heartbit(i_heartbit);
  //Serial.print("Heartbit:");
  //Serial.println(i_heartbit);
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////



//////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////// PROCESS  //////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////


//sensorVal = analogRead(sensorPin);       



//target_time = hora*100;
//target_time = target_time + minu;
int sensorVal=1;

process(20,target_tmp,sensorVal,target_time,current_time);

  //delay(500);
  //digitalWrite(LED_BUILTIN,LOW);
  //delay(500);


//////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////// HW Access Layer ///////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Serial.println(" TANK SERVER ");
//Serial.println(tank_server_out);

if (tank_server_out>3){
  hwa_temperature = random(99);
  delay(2000);
}
else{
  
  http_request("/heater_CMD_update?value="+String(heater_CMD)+"");
  hwa_temperature = http_request("/temperature");
}




}




/////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////// FUNCTIONS //////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////


void epics_layer(){

    
    
    if ( Serial.available()) {
    
      // RECEIVING DATA FROM EPICS
      String InputString = Serial.readStringUntil('\n');
      String DataHeader = InputString.substring(0,5);
      String DataBody;  
  
  
      if (DataHeader == "*STB?") {
        Serial.println(random(999));
      }
    
    //if (InputString == "*STR?") {
      if (DataHeader == "*STR?") {
        Serial.println(DataBody.toInt());
        //Serial.println(DataHeader);
      }

      if (DataHeader == "*HTR!") {
        DataBody = InputString.substring(5,10);
        heater_CMD=DataBody.toInt();
        digitalWrite(LED_BUILTIN,heater_CMD);
        //Serial.println(heater_CMD);
      }
      
      if (DataHeader == "*AUT!") {
        DataBody = InputString.substring(5,10);
        Auto_man=DataBody.toInt();
        //digitalWrite(LED_BUILTIN,heater_CMD);
        //Serial.println(heater_CMD);
      }

      if (DataHeader == "*TME!") {
        DataBody = InputString.substring(5,10);
        current_time = DataBody.toInt();
        //heater_CMD=DataBody.toInt();
        //digitalWrite(LED_BUILTIN,heater_CMD);
        //Serial.println(heater_CMD);
      }
      
      if (DataHeader == "*THR!") {
        DataBody = InputString.substring(5,10);
        target_hour = DataBody.toInt();
        target_time = target_hour*100 + target_min;
        //heater_CMD=DataBody.toInt();
        //digitalWrite(LED_BUILTIN,heater_CMD);
        //Serial.println(heater_CMD);
      }
      if (DataHeader == "*TMN!") {
        DataBody = InputString.substring(5,10);
        target_min = DataBody.toInt();
        target_time = target_hour*100 + target_min;
      }
   
      if (DataHeader == "*TTP!") {
        DataBody = InputString.substring(5,10);
        target_tmp = DataBody.toInt();
      }

    
 
    }

    // SENDING DATA to EPICS
    //Serial.print("tmp,");
    T_EpicsSendData_Current = millis();  //get the current "time" (actually the number of milliseconds since the program started)
    
    if (T_EpicsSendData_Current - T_EpicsSendData_Last >= period_EpicsSendData)  //test whether the period has elapsed
    {
      //int turn;
      switch (turn){
      //if (turn==1){
      case 0:
        Serial.print("tmp,");
        Serial.print(hwa_temperature);
        Serial.print("\r");
        break;
        
      //}
      case 1:
      //if (turn==0){
        Serial.print("tme,");
        //Serial.print(random(99));
        Serial.print(current_time);
        Serial.print("\r");
        break;
      //}

      case 2:
        Serial.print("ttm,");
        //Serial.print(random(99));
        Serial.print(target_time);
        Serial.print("\r");
        break;
      
 
      case 3:
        Serial.print("htb,");
        //Serial.print(random(99));
        Serial.print(i_heartbit);
        Serial.print("\r");
        break;
      }

      
      turn=turn+1;
      if (turn>3){turn=0;}
      
           
                 
      T_EpicsSendData_Last = T_EpicsSendData_Current;  //IMPORTANT to save the start time of the current LED state.
    }

}


int heartbit(int input){

  int output;

  T_Heartbit_Current = millis();  
  // INCREASE
  if (T_Heartbit_Current - T_Heartbit_Last >= period_Heartbit)  //test whether the period has elapsed
  {
    input=input+1;
    T_Heartbit_Last = T_Heartbit_Current;
  }
  
  //RESET
  if (input>=1000){ input=1;}
  output=input;
  //Serial.println(output);
  
  return output;
}





//String getValue(String data, char separator, int index)
//{
//  int found = 0;
//  int strIndex[] = {
//    0, -1  };
//  int maxIndex = data.length()-1;
//  for(int i=0; i<=maxIndex && found<=index; i++){
//    if(data.charAt(i)==separator || i==maxIndex){
//      found++;
//      strIndex[0] = strIndex[1]+1;
//      strIndex[1] = (i == maxIndex) ? i+1 : i;
//    }
//  }
//  return found>index ? data.substring(strIndex[0], strIndex[1]) : "";
//}

///////////////////////////////////////////////////////////////////////////////
////////////////////// PROCES /////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////


int process(int hyst, int temp_target, int temp, int target_time, int current_time)
{
  int hyst_max;
  int hyst_min;
  int wakeup;

  //temp_target = (14/5)*temp_target - 1000;

  hyst_max = temp_target +hyst;
  hyst_min= temp_target -hyst;
  

  if ((current_time>=target_time) && (Auto_man==1)){
  //if (>=1553){
    wakeup=1;
    heater_CMD=0;
    
  }
  else{
    wakeup=0;
    heater_CMD=1;
  }

  
  
  ////////if (wakeup==1){
  ////////  if(temp<hyst_min){digitalWrite(LED_BUILTIN,LOW);}
  ////////  if(temp>hyst_max){digitalWrite(LED_BUILTIN,HIGH);}
  ////////}
  ////////else{
  ////////  digitalWrite(LED_BUILTIN,LOW);
  /////////}
  


  
  return true;

}


////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////
///////////// FUNCTIONS  HWA////////////////////////////

void charToString(char S[], String &D)
{
 
 String rc(S);
 D = rc;
 
}

int http_request(String path){

  int result;
  
  int err =0;
  
  err = http.get(path);
  if (err == 0)
  {
    //Serial.println("startedRequest ok");

    err = http.responseStatusCode();
    if (err >= 0)
    {
      //Serial.print("Got status code: ");
      //Serial.println(err);

      // Usually you'd check that the response code is 200 or a
      // similar "success" code (200-299) before carrying on,
      // but we'll print out whatever response we get

      // If you are interesting in the response headers, you
      // can read them here:
      //while(http.headerAvailable())
      //{
      //  String headerName = http.readHeaderName();
      //  String headerValue = http.readHeaderValue();
      //}

      int bodyLen = http.contentLength();
      
      //Serial.print("Content length is: ");
      //Serial.println(bodyLen);
      //Serial.println();
      //Serial.println("Body returned follows:");
    
      // Now we've got to the body, so we can print it out
      unsigned long timeoutStart = millis();
      //char c;
      char c[10];
      String Tstring;
      int j=0;
      // Whilst we haven't timed out & haven't reached the end of the body
      while ( (http.connected() || http.available()) &&
             (!http.endOfBodyReached()) &&
             ((millis() - timeoutStart) < kNetworkTimeout) )
        if ( (http.connected() || http.available()) &&
             (!http.endOfBodyReached()) &&
             ((millis() - timeoutStart) < kNetworkTimeout) )
 
        {
          if (http.available())
          {
              c[j] = http.read();
                j=j+1;
                           
              // We read something, reset the timeout counter
              timeoutStart = millis();
          }
          else
          {
              // We haven't got any data, so let's pause to allow some to
              // arrive
              delay(kNetworkDelay);
            }
       }
      //Serial.println(c);
      charToString(c,Tstring);
      //Serial.println(Tstring);
      //Serial.println(Tstring.toInt());
      result = Tstring.toInt();
    }
    else
    {    
      Serial.print("Getting response failed: ");
      Serial.println(err);
    }
  }
  else
  {
    Serial.print("Connect failed: ");
    Serial.println(err);
  }
  http.stop();

 
  return result;
  
}




//http_request("/heater_CMD");
//Serial.println(http_request("/heater_CMD"));
//Serial.println(http_request("/temperature"));
//delay (1000);

//http_request("/heater_CMD_update?value=0");
//http_request("/heater_CMD");
//Serial.println(http_request("/heater_CMD"));
