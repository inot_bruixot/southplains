//////////////////////////////////////////////////////////////// LIBRARIES //////////////////////////////////////

///////////////////////////////// UDP Connection //////////////////////////////////////////////////////////////
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////// Temperature Sensor ///////////////////////////////////////////////////////////
// Sensor
#include <DallasTemperature.h>
#include <OneWire.h>
////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////// VARIABLES /////////////////////////////////////

///////////////////////////////// UDP Layer Variable and Constants///////////////////////////////////////////////
// Set WiFi credentials
#define WIFI_SSID_Home "8==D"
#define WIFI_PASS_Home "jakob_cool"
#define WIFI_SSID_Brewery "Funkyland"
#define WIFI_PASS_Brewery "Mr Brown"
#define WIFI_SSID_Nacho "Telia-739C30"
#define WIFI_PASS_Nacho "681AE64353"


#define UDP_PORT 4210

// UDP Layer Variables
WiFiUDP UDP;
char udp_cPacket_notused[25];
//char reply[25] = "Packet received!";
//char udp_reply_notused[25];
//String reply;

const int udp_iUDPPort_202 = 4202;
const int udp_iUDPPort_201 = 4201;
const int udp_iUDPPort_203 = 4203;
int udp_iUDPPort_Target;

// UDP Nomenclature
const String udp_sTank_tmp = "TMP?";      // UPD Temperature Request
const String udp_sHeater_rbk = "HTR?";     // UPD Heater Readback
const String udp_sHeater_cmd = "HTR!";
const String ESP8266_201_MAC = "E8:DB:84:C5:8C:B2";
const String ESP8266_202_MAC = "BC:DD:C2:30:8E:33";
const String ESP8266_203_MAC = "30:83:98:A2:C6:89";

///////////////////////////////// Hardware Access Layer Variables /////////////////////////////////////////////
// Hardware Access Layer Variables
int hwi_iTank_tmp;                     // Temperature of the tank
bool hwi_bHeater_rbk;                   // Heater Readback
bool hwi_bHeater_cmd;                   // Heater Command

// Temperature Sensor Variables
int   pin_number_sensor = 12;
//float f_temperature = 0;             //Variable to store the temperature in

OneWire oneWirePin(pin_number_sensor);
DallasTemperature sensor(&oneWirePin);

///////////////////////////////// Time Variables /////////////////////////////////////////////////////////////
// Time variables
const unsigned long period = 10000;  //the value is a number of milliseconds
unsigned long startMillis;  //some global variables available anywhere in the program
unsigned long currentMillis;
int elapsed_time;


////////////////////////////////////// General Variables  /////////////////////////////////////////////////////

bool LED_STATE;
const   int HTR_PIN = 5;
const   int TMP_PIN = 12;


/////////////////////////////////////////// IP Variables //////////////////////////////////////////////////////
// Definition of the IP
// Set your Static IP address

//IPAddress local_IP(192, 168, 1, 202);

// Set your Gateway IP address
IPAddress gateway(192, 168, 1, 1);
IPAddress subnet(255, 255, 255, 0);

IPAddress Blue_IP(192, 168, 1, 212);


void setup() {

  //////////////////////  Set-up Outputs //////////////


  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(HTR_PIN, OUTPUT);
  pinMode(TMP_PIN, INPUT);


  /////////////////////////////////////////////////////

  // Setup serial port
  Serial.begin(115200);
  Serial.println();

  /////////////////////////////////////////////////////
  IPAddress ESP8266_201_IP(192, 168, 1, 201);
  IPAddress ESP8266_202_IP(192, 168, 1, 202);
  IPAddress ESP8266_203_IP(192, 168, 1, 203);
  
  IPAddress Target_IP;


  
  
  
  // Assign the IP Address + PORT depending on the MAC of ESP8266
  //Serial.println("HERE i am ");
  if (WiFi.macAddress() == ESP8266_201_MAC) {
    Target_IP = ESP8266_201_IP;
    udp_iUDPPort_Target = udp_iUDPPort_201;
  }
  if (WiFi.macAddress() == ESP8266_202_MAC) {
    Target_IP = ESP8266_202_IP;
    udp_iUDPPort_Target = udp_iUDPPort_202;
  }
  if (WiFi.macAddress() == ESP8266_203_MAC) {
    Target_IP = ESP8266_203_IP;
    udp_iUDPPort_Target = udp_iUDPPort_203;
  }
  
  
  
  // Configures static IP address
  //if (!WiFi.config(local_IP, gateway, subnet, primaryDNS, secondaryDNS)) {
  if (!WiFi.config(Target_IP, gateway, subnet)) {
    Serial.println("Static IP Failed to configure");
  }
  else {
    Serial.println("Static IP Configured");
  }


//////////////////////////////////////////// WIFI Connection ///////////////////////////////////////////////////////////////////////////////////////////////


  //String WIFI_SSID_Target = WIFI_SSID_Nacho;
  //String WIFI_PASS_Target = WIFI_PASS_Nacho;
  // Declaration of WIFI
  char *WIFI_SSID_Target[]={WIFI_SSID_Nacho, WIFI_SSID_Brewery, WIFI_SSID_Home, "This is string 4", "This is string 5", "This is string 6"};
  char *WIFI_PASS_Target[]={WIFI_PASS_Nacho, WIFI_PASS_Brewery, WIFI_PASS_Home, "This is string 4", "This is string 5", "This is string 6"};



  //Serial.println (NUMITEMS (WIFI_SSID_Target));
  //Serial.println ("ello");
  //Serial.println (sizeof (WIFI_SSID_Target));

  for (int i_wifi = 0; i_wifi <= 4; i_wifi++) {
    // Begin WiFi
    WiFi.begin(WIFI_SSID_Target[i_wifi], WIFI_PASS_Target[i_wifi]);

    // Connecting to WiFi...
    Serial.println();
    Serial.print("Connecting to ");
    Serial.println(WIFI_SSID_Target[i_wifi]);
    Serial.println("MAC Address:" + WiFi.macAddress());
    Serial.println(WIFI_SSID_Target[i_wifi]);
    Serial.println(WIFI_PASS_Target[i_wifi]);

    // Loop continuously while WiFi is not connected
    int iConn_attempt=0;
    while (WiFi.status() != WL_CONNECTED and iConn_attempt<7)
    {
      delay(1000);
      Serial.print(".");
      iConn_attempt=iConn_attempt+1;
    }

    // Connected to WiFi
    if (WiFi.status() == WL_CONNECTED){
      Serial.println();
      Serial.print("Connected! IP address: ");
      Serial.println(WiFi.localIP());
      break;
    }
    else{
      Serial.println("Not Connected");
    }

  } // End_for


//////////////////////////////////////////// WIFI Connection ///////////////////////////////////////////////////////////////////////////////////////////////



  // Begin listening the UDP port
  UDP.begin(UDP_PORT);
  Serial.print("Listening on UDP port ");
  Serial.println(UDP_PORT);



}

void loop() {


  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////// TEST ///////////////////////////////////////////
  //blinking_led();


  ////////////////////////////// UDP LAYER ////////////////////////////////////////////
  // UDP Layer
  // IMPROVEMENT of this FUNCTION is neeeded !!!!!! Create a Array of INPUTS and treat the RETURN of the Function
  //parse_udp_data(udp_sTank_tmp,hwi_iTank_tmp);
  //parse_udp_data(udp_sHeater_rbk,hwi_bHeater_rbk);
  parse_udp_data();

  //////////////////////////// PROCESS TEST ///////////////////////////////////////////
  // TESSSSSSSSTTTTTTTTTTTTT
  // MOVING DATAAAAAAAAAAAA
  hwi_iTank_tmp = get_tmp();



  hwi_bHeater_rbk = !digitalRead(LED_BUILTIN);


  // CICLE TIME
  currentMillis = millis();  //get the current "time" (actually the number of milliseconds since the program started)
  elapsed_time = currentMillis - startMillis;
  Serial.println(String(elapsed_time));
  startMillis = currentMillis;





  /////////////////////////////////////////////////////////////////////////////////////



  /////////////////////////////// HARDWARE ACCESS LAYER ////////////////////////////////
  if (hwi_bHeater_cmd == 0) {
    digitalWrite(LED_BUILTIN, HIGH);
  }
  //digitalWrite(HTR_PIN,LOW);}
  if (hwi_bHeater_cmd == 1) {
    digitalWrite(LED_BUILTIN, LOW);
  }
  //digitalWrite(HTR_PIN,HIGH);}






}





//////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////// FUNCTIONS /////////////////////////////////

//int parse_udp_data(String udp_var,int hwi_var){
void parse_udp_data() {

  /////////////////////////////////////////////////////////
  // Definition of Variables
  char cPacket[25];
  char cReply[25];
  String sPacket;
  String sReply;

  String sVal;
  String sData;


  int iPos_type;
  int iPos_star;
  int iPos_query;
  int iPos_admir;
  int iPos_equal;


  //////////////////////////////////////////////FUNCTIONS ///////////////////////////////////////////////////////////
  // If packet received...
  int iPacketSize = UDP.parsePacket();

  //Serial.println(packetSize);

  if (iPacketSize) {
    Serial.print("Received packet! Size: ");
    Serial.println(iPacketSize);
    int len = UDP.read(cPacket, 255);
    if (len > 0)
    {
      cPacket[len] = '\0';
    }
    Serial.print("Packet received: ");
    Serial.println(cPacket);


    // GET DATA ////////////////////////////////////////////
    sPacket = String(cPacket);
    Serial.println(sPacket);

    iPos_star = sPacket.indexOf('*');
    iPos_query = sPacket.indexOf('?');
    iPos_admir = sPacket.indexOf('!');
    iPos_equal =  sPacket.indexOf('=');
    if (iPos_query > 0) {
      sData = sPacket.substring(iPos_star + 1, iPacketSize);
    }
    else {
      sData = sPacket.substring(iPos_star + 1, iPos_equal);
    }

    // GREEN = LIGHT --> If we have STAR
    ////////////////////////////////////////////////////////


    // TRIAGE
    Serial.print("DATA:");
    Serial.println(sData);
    Serial.println("Is HTR her:" + String(sData.indexOf(udp_sHeater_cmd)));

    if (sData == udp_sTank_tmp) {
      //sReply=String("*"+udp_sTank_tmp+"="+hwi_iTank_tmp);
      sReply = String(hwi_iTank_tmp);
      //sReply=String("*"+udp_var+"="+hwi_var);
      ///////////sReply=String(hwi_iTank_tmp);
      goto end_triage;
    }
    if (sData == udp_sHeater_rbk) {
      //sReply=String("*"+udp_sHeater_rbk+"="+hwi_bHeater_rbk);
      sReply = String(hwi_bHeater_rbk);
      //////////sReply=String(hwi_bHeater_rbk);
      goto end_triage;
    }

    //if (sData==udp_sHeater_cmd){
    if ((sData.indexOf(udp_sHeater_cmd) != -1)) {
      //sVal = sPacket.substring(iPos_equal+1,iPacketSize);
      sVal = sPacket.substring(iPos_admir + 1, iPacketSize);
      hwi_bHeater_cmd = sVal.toInt();
      Serial.println("SVAL:" + sVal);
      //sReply=String("*"+udp_sHeater_cmd+"="+"OK");
      sReply = String("OK");
      ///////sReply=String("OK");

      goto end_triage;
    }
    else {
      sReply = String(-1);
    }

end_triage:



    sReply.toCharArray(cReply, 25);
    UDP.beginPacket(UDP.remoteIP(), udp_iUDPPort_Target);

    Serial.println(UDP.remoteIP());
    Serial.println(UDP.remotePort());
    Serial.println(sReply);
    UDP.write(cReply);
    UDP.endPacket();

    // Trying network
    //UDP.beginPacket(Blue_IP, udp_iUDPPort);

    //Serial.println("192.168.1.212");
    //Serial.println(UDP.remotePort());
    //Serial.println(sReply);
    //UDP.write(cReply);
    //UDP.endPacket();



  }

  // To implement in the future
  //return 1;
}

void blinking_led() {

  currentMillis = millis();  //get the current "time" (actually the number of milliseconds since the program started)

  if (currentMillis - startMillis >= period) { //test whether the period has elapsed

    if (LED_STATE == 1) {
      digitalWrite(LED_BUILTIN, LOW);
      LED_STATE = 0;
      Serial.println("false");
    } else {
      digitalWrite(LED_BUILTIN, HIGH);
      LED_STATE = 1;
      Serial.println("true");
    }
    Serial.println("TEMP:");
    Serial.println(hwi_iTank_tmp);

    startMillis = currentMillis;  //IMPORTANT to save the start time of the current LED state.
  }
}












int get_tmp() {
  int tmp;

  sensor.requestTemperatures();
  //tmp=sensor.getTempCByIndex(0);
  tmp = random(100);

  return tmp;

}
