
/*
  Epics_udp.h - Library for flashing Epics_udp code.
  Created by David A. Mellis, November 2, 2007.
  Released into the public domain.
*/
#ifndef Epics_udp_h
#define Epics_udp_h

#include "Arduino.h"
#include <WiFiUdp.h>

class Epics_udp
{
  public:
    Epics_udp(int pin);
    void dot();
    void dash();
	void BlinkLed(int freq);
	String Triage(char *udp_sFunRequest[],String sFunData_received,char *hwi_sFunField[]);
	void Send_udp_data(String sFunReply,  WiFiUDP UDP_Fun, int iFun_UDPPort_Target);
	String Parse_udp_data(WiFiUDP UDP_Fun);
	void Wifi_Conn(char *WIFI_SSID_Target_Fun[],char *WIFI_PASS_Target_Fun[]);
	int Wifi_Conf(String Fun_WIFI_MACs[],IPAddress Fun_WIFI_IPs[],int Fun_WIFI_UDPs[], IPAddress Fun_Gateway, IPAddress Fun_Subnet);
  private:
    int _pin;
};



#endif
