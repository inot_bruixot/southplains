
/*
  Epics_udp.cpp - Library for flashing Epics_udp code.
  Created by David A. Mellis, November 2, 2007.
  Released into the public domain.
*/

#include <Arduino.h>
#include <WiFiUdp.h>
#include <ESP8266WiFi.h>

#include <Epics_udp.h>

Epics_udp::Epics_udp(int pin)
{
  pinMode(pin, OUTPUT);
  _pin = pin;
}

void Epics_udp::dot()
{
  digitalWrite(_pin, HIGH);
  delay(250);
  digitalWrite(_pin, LOW);
  delay(250);  
}

void Epics_udp::dash()
{
  digitalWrite(_pin, HIGH);
  delay(1000);
  digitalWrite(_pin, LOW);
  delay(250);
}

void Epics_udp::BlinkLed(int freq)
{
	  
    digitalWrite(LED_BUILTIN,!digitalRead(LED_BUILTIN));
	delay(freq);
}

String Epics_udp::Triage(char *udp_sFunRequest[],String sFunData_received,char *hwi_sFunField[]){
 
  String sFunReply;

  int i=0;
  while (udp_sFunRequest[i] != "END"){
      // REQUEST of DATA : sFunData_received contains ? --
      if ((sFunData_received.indexOf("?")!=-1) and sFunData_received == udp_sFunRequest[i]) {  // Data Request CAGET  // 0. *TMP? // 1. *HTR? // etc
          sFunReply=hwi_sFunField[i]; 
          goto end_triage;
      }
      // ASSIGNMENT of DATA : sFunData_received contains ! --
      if ((sFunData_received.indexOf("!")!=-1) and (sFunData_received.indexOf(udp_sFunRequest[i])) != -1) {  // Data Assignment CAPUT // 2. *HTR! // etc
          ////sVal = sPacket.substring(iPos_admir + 1, iPacketSize);
          ////hwi_bHeater_cmd = sVal.toInt();
          ////Serial.println("SVAL:" + sVal);
          //sFunReply=hwi_sFunField[i];
          sFunReply="OK";
          goto end_triage;
      }
      else {
          sFunReply="DATA_UNKNOWN";
          i=i+1;
      }
  }

  end_triage:
  return sFunReply;
}


void Epics_udp::Send_udp_data(String sFunReply, WiFiUDP UDP_Fun, int iFun_UDPPort_Target){

    //WiFiUDP UDP_Fun;
	char cFunReply[25];
    IPAddress Blue_IP_Fun(192, 168, 1, 211);
	//iFun_UDPPort_Target=4203;
        
    sFunReply.toCharArray(cFunReply, 25);
    UDP_Fun.beginPacket(UDP_Fun.remoteIP(), iFun_UDPPort_Target);

    Serial.println(UDP_Fun.remoteIP());
    Serial.println(UDP_Fun.remotePort());
    Serial.println(sFunReply);
    UDP_Fun.write(cFunReply);
    UDP_Fun.endPacket();
    yield();

    // TESTING
    UDP_Fun.beginPacket(Blue_IP_Fun, iFun_UDPPort_Target);
	//UDP_Fun.beginPacket(Blue_IP_Fun, 4203);
    Serial.println(Blue_IP_Fun);
    Serial.println(UDP_Fun.remotePort());
    Serial.println(sFunReply);
    UDP_Fun.write(cFunReply);
    UDP_Fun.endPacket();
    yield();

 }
 
String Epics_udp::Parse_udp_data(WiFiUDP UDP_Fun){
  
  char cFunPacket[25];
  String sFunPacket="0";
  

  // If packet received...
  int iFunPacketSize = UDP_Fun.parsePacket();
 
  
  if (iFunPacketSize) {
    //Serial.print("TPARSE Received packet! Size: ");
    //Serial.println(iPacketSize);
    int len = UDP_Fun.read(cFunPacket, 255);
    if (len > 0)
        {
          cFunPacket[len] = '\0';
        }
    //Serial.print("TPARSE Packet received: ");
    sFunPacket = String(cFunPacket);
    //Serial.println(sPacket);
  }
  else{
    //Serial.print("TPARSE NOTHING ");
    //Serial.println(sPacket);
  }

  return sFunPacket;
}


void Epics_udp::Wifi_Conn(char *WIFI_SSID_Target_Fun[],char *WIFI_PASS_Target_Fun[]){

  int i_wifi=0;
  while (WIFI_SSID_Target_Fun[i_wifi] != "END"){
  //for (int i_wifi = 0; i_wifi <= 4; i_wifi++) {
    // Begin WiFi
    WiFi.begin(WIFI_SSID_Target_Fun[i_wifi], WIFI_PASS_Target_Fun[i_wifi]);

    // Connecting to WiFi...
    Serial.println();
    Serial.print("Connecting to ");
    Serial.println(WIFI_SSID_Target_Fun[i_wifi]);
	Serial.println(WIFI_PASS_Target_Fun[i_wifi]);
    //Serial.println(WIFI_PASS_Target_Fun[i_wifi]);
    Serial.println("MAC Address:" + WiFi.macAddress());
    


    // Loop continuously while WiFi is not connected
    int iConn_attempt=0;
    while (WiFi.status() != WL_CONNECTED and iConn_attempt<7)
    {
      delay(1000);
      Serial.print(".");
      iConn_attempt=iConn_attempt+1;
    }

    // Connected to WiFi
    if (WiFi.status() == WL_CONNECTED){
      Serial.println();
      Serial.print("Connected! IP address: ");
      Serial.println(WiFi.localIP());
      break;
    }
    else{
      Serial.println("Not Connected");
    }

  i_wifi++;
  } // End_while

}


int Epics_udp::Wifi_Conf(String Fun_WIFI_MACs[],IPAddress Fun_WIFI_IPs[],int Fun_WIFI_UDPs[], IPAddress Fun_Gateway, IPAddress Fun_Subnet){

  IPAddress Fun_Target_IP;
  int Fun_Target_iUDPPort;
  int Fun_i;
  // Assign the IP Address + PORT depending on the MAC of ESP8266

  Serial.println("WIFI CONF FUNCTION");
  
  //if (WiFi.macAddress() == ESP8266_201_MAC) {
  Fun_i=0;
  while (Fun_WIFI_MACs[Fun_i]!="END"){
      if (WiFi.macAddress() == Fun_WIFI_MACs[Fun_i]) {
        Fun_Target_IP = Fun_WIFI_IPs[Fun_i];
        Fun_Target_iUDPPort = Fun_WIFI_UDPs[Fun_i];
        Serial.println("I AM insidev : "+String(Fun_i));
        Serial.println(WiFi.macAddress());
        Serial.println(Fun_WIFI_MACs[Fun_i]);
 
        break;
      }
      Fun_i++;
  }
  Serial.println(Fun_Target_IP);
  Serial.println(Fun_Target_iUDPPort);

  //if (WiFi.macAddress() == ESP8266_202_MAC) {
  //  Fun_Target_IP = ESP8266_202_IP;
  //  Fun_Target_iUDPPort = udp_iUDPPort_202;
  //}
  //if (WiFi.macAddress() == WIFI_MACs[2]) {
  //  Fun_Target_IP = ESP8266_203_IP;
  //  Fun_Target_iUDPPort = udp_iUDPPort_203;
  //}
  
  
  
  // Configures static IP address
  //if (!WiFi.config(local_IP, gateway, subnet, primaryDNS, secondaryDNS)) {
  if (!WiFi.config(Fun_Target_IP, Fun_Gateway, Fun_Subnet)) {
    Serial.println("Static IP Failed to configure");
  }
  else {
    Serial.println("Static IP Configured");
  }

  return Fun_Target_iUDPPort;
}



