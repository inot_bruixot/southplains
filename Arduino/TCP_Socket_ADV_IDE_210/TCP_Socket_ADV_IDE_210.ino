/*
   Copyright (c) 2018, circuits4you.com
   All rightOs reserved.
   Create a TCP Server on ESP8266 NodeMCU.
   TCP Socket Server Send Receive Demo
*/



////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////// MAIN LIBRARIES ///////////////////////////////////////////////////////////////

#include <ESP8266WiFi.h>
//#include <Epics_udp.h>
#include <epics_com.h>

#define SendKey 0  //Button to send data Flash BTN on NodeMCU

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////// Temperature Sensor ///////////////////////////////////////////////////////////
// Sensor
#include <DallasTemperature.h>
#include <OneWire.h>

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////// Server Object Declaration /////////////////////////////////////////////////////
int default_port = 8888;  //Port Default number
WiFiServer server(default_port);
//WiFiServer my_server(default_port);


// TESTING VAR
//int count = 0;                     
//int temp = 0;
//int num = 0;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// EPICS TCP TX
int i_index = 0;
bool b_fetch;
String s_tx_completed;
int i_tx_completed;
char c_mybuffer[6];
char result[4] = {0};

// Status = waiting , fetching
// Type = request, command
// Index = 0 .. 4
// Message = ""

//{"Status", "Type", "Index","Message"};
//String static tx_data[]  = {"Waiting", "Request", "0",""};
String static *tx_data;
String static *triage_data;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////  Constants/////////////////////////////////////////////////////////////////
// Set WiFi credentials
char WIFI_SSID_Home[] ="COMHEM_a08627";
char WIFI_PASS_Home[] ="mmzmjdnd";
//#define WIFI_SSID_Brewery "Funkyland 2.5ghz"
//#define WIFI_PASS_Brewery "Mr Brown"
//#define WIFI_SSID_Nacho "Telia-739C30"
//#define WIFI_PASS_Nacho "681AE64353"
//#define WIFI_SSID_Pau "Telia-615DFB"
//#define WIFI_PASS_Pau "D2DCBF3DE2"
//#define WIFI_SSID_Temp1 "iPhoneESS"
//#define WIFI_PASS_Temp1 "P10204251e"
  char WIFI_SSID_Mamuts[] ="MOVISTAR_AA";
char WIFI_PASS_Mamuts[] ="938332193";
//#define WIFI_SSID_Marta "MOVISTAR_C4E9"
//#define WIFI_PASS_Marta "4GRBidha4xip7NyQGxSt"
char END[]="END";

// Declaration of WIFI
char *WIFI_SSID_Target[] = {WIFI_SSID_Mamuts, WIFI_SSID_Home, END};
char *WIFI_PASS_Target[] = {WIFI_PASS_Mamuts, WIFI_PASS_Home, END};

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////// ESP 8266 CONFIGURATION  /////////////////////////////////////////////////////////////////
//const String ESP8266_21_MAC = "F4:CF:A2:E4:A8:7F";
//const String ESP8266_21_MAC = "E8:DB:84:C5:8C:B2";
const String ESP8266_21_MAC = "C8:C9:A3:0C:03:4A";

//const String ESP8266_202_MAC = "BC:DD:C2:30:8E:33";
const String ESP8266_22_MAC = "E8:DB:84:C5:8C:B2";
const String ESP8266_23_MAC = "F4:CF:A2:E4:81:00";
const String ESP8266_24_MAC = "30:83:98:B5:53:F5";
const String ESP8266_31_MAC = "C8:C9:A3:0C:04:5E";

String WIFI_MACs[] = {ESP8266_21_MAC, ESP8266_22_MAC, ESP8266_23_MAC, ESP8266_24_MAC, ESP8266_31_MAC, "END"};


const IPAddress ESP8266_21_IP(192, 168, 0, 21);
const IPAddress ESP8266_22_IP(192, 168, 0, 22);
const IPAddress ESP8266_23_IP(192, 168, 0, 23);
const IPAddress ESP8266_24_IP(192, 168, 0, 24);
const IPAddress ESP8266_31_IP(192, 168, 0, 31);
IPAddress WIFI_IPs[] = {ESP8266_21_IP, ESP8266_22_IP, ESP8266_23_IP, ESP8266_24_IP, ESP8266_31_IP};

// Set your Gateway IP address
//IPAddress gateway(192, 168, 1, 1);
IPAddress gateway(192, 168, 0, 1);
IPAddress subnet(255, 255, 255, 0);

const int tcp_iUDPPort_21 = 8021;
const int tcp_iUDPPort_22 = 8022;
const int tcp_iUDPPort_23 = 8023;
const int tcp_iUDPPort_24 = 8024;
const int tcp_iUDPPort_31 = 8031;
int WIFI_TCP_Ports[] = {tcp_iUDPPort_21, tcp_iUDPPort_22, tcp_iUDPPort_23, tcp_iUDPPort_24, tcp_iUDPPort_31};
int WIFI_TCP_Port_Target;


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// DECLARE Constructor Class epics_com
//Epics_udp epics_udp(13); // Change the Name of this function
epics_com epics_com(13);

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///// CONSTANTS
const   int HTS_PIN = 4;
const   int HTR_PIN = 5;
const   int TMP_PIN = 12;

///// COMUNICATION MESSAGES 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
WiFiClient tcp_client;
String tx_sRequestsType[]  = {"*TMP?", "*HTR?", "*HTR!", "*TMS!", "*TSS!", "*UPL!", "END"};
String hwi_sFieldStatus[] = {"0", "0", "0", "0", "0", "0", "Null"}; // Initialization


///////////////////////////////////////////////////////////////////////////////////////////////////////////
// Temperature Sensor Variables
//int   pin_number_sensor = 12;

OneWire oneWirePin(TMP_PIN);
DallasTemperature sensor(&oneWirePin);

///////////////////////////////// Time Variables /////////////////////////////////////////////////////////////
// Time variables
const unsigned long period = 10000;  //the value is a number of milliseconds
unsigned long startMillis;  //some global variables available anywhere in the program
unsigned long currentMillis;
int elapsed_time;


//=======================================================================
//                    Power on setup
//=======================================================================
void setup()
{

  //////////////////////  Set-up Input/Outputs //////////////


  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(HTR_PIN, OUTPUT);
  //pinMode(TMP_PIN, INPUT);
  //pinMode(HTS_PIN, INPUT);

  ////////////////////// Serial /////////////////////////
  Serial.begin(115200);
  pinMode(SendKey, INPUT_PULLUP); //Btn to send data
  Serial.println();

  // Wifi Configuration
  WIFI_TCP_Port_Target = epics_com.Wifi_Conf_TCP(WIFI_MACs, WIFI_IPs, WIFI_TCP_Ports, gateway, subnet);
  
  // Wifi Connection
  //Wifi_Conn_TCP(WIFI_SSID_Target, WIFI_PASS_Target);
  epics_com.Wifi_CoRn_TCP(WIFI_SSID_Target, WIFI_PASS_Target);
  
  // Wifi TCP Server ON
  Wifi_ON_Server_TCP(WIFI_TCP_Port_Target);
  //epics_com.Wifi_ON_ServRer_TCP(WIFI_TCP_Port_Target, server);

}

//// Var Time Elapsed
//uint16_t time_elapsed=0;
uint16_t time_elapsed_original;
uint16_t time_elapsed_original_rest;
uint16_t time_elapsed_current;

// HAL Time
uint16_t current_time_HAL;



//=======================================================================================================================
//                    Loop
//=======================================================================================================================

void loop()

{
  //WiFiClient tcp_client = server.available();

  //WiFiClient my_client;


  //char mytest;
  String *Test_String;


  String sTCP_data;
  String DataCompare = "*TMP?";
  int hwi_test = 0;
  String shwi_test;
  //bool b_fetch;
  //bool i_tx_completed;
  //String sTCP_reply;
  int iTCP_reply;
  char tx;
  int b_help;
  //int temp=0;

  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////// TCP CONNECTION /////////////////////////////////////////////
  tcp_client = server.available();

  if (tcp_client) {
    if (tcp_client.connected())
    {
      Serial.println("Client Connected");
    }
    while (tcp_client.connected())
    {
      while (tcp_client.available() > 0)
      {
        
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////// EPICS-TCP LAYER ////////////////////////////////////////////////
        // read data from the connected client
        char tx = tcp_client.read();

        Serial.println("--------------------------------------");
        Serial.println("--------------------------------------");

        //PARSING DATA RECEIVED
        tx_data = epics_com.Parse_3_tcp_data(tx);
        
        // EXTRACTION of DATA
        sTCP_data = tx_data[3];
        s_tx_completed = tx_data[4];
        i_tx_completed = s_tx_completed.toInt();

        // TEMPORARY
        Serial.print("MESSAGE : ");
        Serial.println(sTCP_data);
        Serial.print("COMPLETED STRING");
        Serial.println(s_tx_completed);
        Serial.print("COMPLETED INT");
        Serial.println(i_tx_completed);


        // TRIAGE
        iTCP_reply = Triage_TCP(sTCP_data, tx_sRequestsType, hwi_sFieldStatus);

        // PLC Core Data Access
        triage_data = Triage_3_TCP(tx_data);
        Serial.print("first:");
        Serial.println(triage_data[0]);
        Serial.print("Second:");
        Serial.println(triage_data[1]);





        Serial.print("This is the reply: ");

        // REPLY
        if (i_tx_completed==1)
        {
          Conv_Ascii(iTCP_reply);
          //Conv_Ascii(77);
          
          Serial.print("This is the reply: ");
          Serial.println(iTCP_reply);
          Serial.println("Client Write");
          tcp_client.write(result);
        }

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////// TIME CHECK////////////////////////////////////////////

        time_elapsed_original = millis();
        time_elapsed_original_rest = time_elapsed_original - time_elapsed_current;
        Serial.print("TIME ELAPSED :  ");
        Serial.print(time_elapsed_original_rest);
        Serial.println(" ms");

        time_elapsed_current = millis();
        //delay(1000);

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////// PROCES ///////////////////////////////////////////////

        //hwi_test=round(get_tmp()/10);

        //hwi_test=3;
        if (i_tx_completed == 0) {
          b_help = 1;
        }
        if (i_tx_completed == 1 and b_help == 1) {
          hwi_test++;
          b_help = 0;
          // One delay for MESSAGE COMPLETED
          //delay(1500);
          //hwi_test=round(get_tmp()/10);
          //round(get_tmp() / 10);

        }
        if (hwi_test > 99) {
          hwi_test = 0;
        }

        //Serial.print(" this is HWI test");
        //Serial.println(hwi_test);
        hwi_sFieldStatus[0] = String(hwi_test);
        //hwi_test=random(2);
        hwi_sFieldStatus[1] = String(random(2));

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////// HAL //////////////////////////////////////////////////

        //current_time_HAL = millis();
        HAL(i_tx_completed);


        //tx_data = epics_com.Parse_3_tcp_data(tx);
        Serial.println(tx_data[0]);
        Serial.println(tx_data[1]);
        Serial.print("Index :");
        Serial.println(tx_data[2]);
        Serial.println(tx_data[3]);
        Serial.print("Completed :");
        Serial.println(tx_data[4]);
        
        
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

      }
    }

    tcp_client.stop();
    Serial.println("Client disconnected");
  }

}


//============================================================================================================================
//============================================================================================================================













//============================================================================================================================
//============================================================================================================================

void HAL(bool i_tx_completed_fun) {
  
  
  Serial.print("Hwi_0 :");
  Serial.println(hwi_sFieldStatus[0]);
  //Conv_Ascii(hwi_sFieldStatus[0]);
  //Serial.print("Hwi_Converted :");
  //Conv_Ascii(hwi_sFieldStatus[0].toInt());


  Serial.print("Hwi_1 :");
  Serial.println(hwi_sFieldStatus[1]);
  Serial.print("Hwi_2 :");
  Serial.println(hwi_sFieldStatus[2]);

  digitalWrite(LED_BUILTIN, !hwi_sFieldStatus[2].toInt());
  digitalWrite(HTR_PIN, hwi_sFieldStatus[2].toInt());

  if (i_tx_completed_fun){ get_tmp(); }
  
}


void Conv_Ascii(int num) {

  int i_unidad;
  int i_decena;

  //result[0] = num + '0';  // convert to ascii and store
  i_unidad   = num % 10;
  i_decena = num / 10 ;
  result[0] = i_decena + '0';
  result[1] = i_unidad + '0';
 

  //result[0] = '3';  // convert to ascii and store
  //result[1] = '4';
  //Serial.print(result);  // prints "345"
  //return result;

}


void Wifi_ON_Server_TCP(int port)
{
  //int port = 8882;  //Port number
  //WiFiServer server(port);
  //server(port);
  server.begin(port);
  Serial.println("TCP Server Started");
  Serial.println("Open Telnet and connect to IP:");


}

String Parse_tcp_data(char c) {

  String sTCP_data_fun;
  int iSize_query = 4;


  if (c == '*')
  {
    Serial.println(" * MESSAGE STARTED ");
    b_fetch = true;
  }

  // if the last char is ! then "te doy otra ronda"
  if ((i_index <= iSize_query or c_mybuffer[i_index - 1] == '!') and b_fetch == true)
    //if(strlen(c_store)>0 and b_fetch==true)
  {
    c_mybuffer[i_index] = c;
    //Serial.println(String(c_mybuffer));
    i_index++;
    //Serial.println(index);
    //mybuffer[index] = '\0';
    i_tx_completed = false;
  }

  // if the last char is ! then "te doy otra ronda"
  if (i_index > iSize_query and b_fetch == true and c_mybuffer[i_index - 1] != '!') // We must be sure we still lisen the  COMMAND '!'
    //if(strlen(c_store)==0 and b_fetch==true)
  {
    sTCP_data_fun = String(c_mybuffer);
    c_mybuffer[i_index] = '\0';

    // Empty buffer
    c_mybuffer[0] = '\0';
    c_mybuffer[1] = '\0';
    c_mybuffer[2] = '\0';
    c_mybuffer[3] = '\0';
    c_mybuffer[4] = '\0';


    i_index = 0;
    b_fetch = false;
    i_tx_completed = true;
    Serial.println(" MESSAGE COMPLETED ");
  }

  return sTCP_data_fun;
}




int Triage_TCP(String sFunData_received, String udp_sFunRequestsType[], String hwi_sFunFieldStatus[]) {

  String sFunReply;
  int iFunReply;
  int iPos_admir;
  int iPacketSize;
  String sVal;
  String sData;

  int i = 0;
  while (udp_sFunRequestsType[i] != "END") {


    // REQUEST of DATA : sFunData_received contains ? --
    if ((sFunData_received.indexOf("?") != -1) and sFunData_received == udp_sFunRequestsType[i]) { // Data Request CAGET  // 0. *TMP? // 1. *HTR? // etc
      sFunReply = hwi_sFunFieldStatus[i];
      iFunReply = sFunReply.toInt();
      goto end_triage;
    }


    // COMMAND of Data : It contains ! and Matches One of the Commands
    if ((sFunData_received.indexOf("!") != -1) and (sFunData_received.indexOf(udp_sFunRequestsType[i])) != -1) { // Data Assignment CAPUT // 2. *HTR! // etc

      iPacketSize = sFunData_received.length();
      iPos_admir = sFunData_received.indexOf('!');
      sVal = sFunData_received.substring(iPos_admir + 1, iPacketSize);

      hwi_sFunFieldStatus[i] = sVal;
      //hwi_sFunFieldStatus[i] = "3";
      

      //sFunReply="1";
      //iFunReply = 1;
      //iFunReply = hwi_sFunFieldStatus[i].toInt();
      iFunReply = sVal.toInt();

      goto end_triage;
    }
    else {
      iFunReply = 0;
      i = i + 1;
    }
  }

end_triage:
  return iFunReply;

}

////////////////////////////////////////////////////////////////////////////////////////////////////////7
////////////////////////////////////////////////////////////////////////////////////////////////////////7

int get_tmp() {
  int tmp;

  sensor.requestTemperatures();
  tmp = sensor.getTempCByIndex(0);
  //tmp = random(100);
  Serial.print(" Temperature :");
  Serial.println(tmp);

  return tmp;

}

String * Triage_3_TCP(String *tx_data_fun)
{
  String first;
  String Second;
  String Third;
  static String triage_data_fun[2]={"0","0"}; // Match , Reply

  //  tx_data_fun[0]=Status;
  //  tx_data_fun[1]=Type;
  //  tx_data_fun[2]=String(Index);
  //  tx_data_fun[3]= 
	//  tx_data_fun[4]=String(Completed);
  
  //Serial.print("First :");
  //Serial.println(tx_data_fun[0]);
  //Serial.print("Second :");
  //Serial.println(tx_data_fun[1]);
  //Serial.print("Third :");
  //Serial.println(tx_data_fun[2]);

  triage_data_fun[0]="hola";
  triage_data_fun[1]="hsdfdsfola";


  return triage_data_fun;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////7
////////////////////////////////////////////////////////////////////////////////////////////////////////7
//String * BuildArray (void)
//{
//    int MyArray[10]={0,1,2,3,4,5,6,7,8,9};
//    struct response {int be; int two;};
//    static String MyString[3]={"1","0","candemor"};
//    //return MyString;
//    return MyString;
//
//}   

//String * Parce_2_tcp_data(String tx_data_in_fun[])
//{
//   //Serial.print(" Temperature :");
//   //static String tx_data_out_fun[3]={"hello","bye","ds"};
//   static String tx_data_out_fun[3];
//   tx_data_out_fun[0]=tx_data_in_fun[0];
//   tx_data_out_fun[1]=tx_data_in_fun[1];
//
//   return tx_data_out_fun;
//   //return MyString;
//}

