
/*
 * Copyright (c) 2018, circuits4you.com
 * All rightOs reserved.
 * Create a TCP Server on ESP8266 NodeMCU. 
 * TCP Socket Server Send Receive Demo
*/

#include <ESP8266WiFi.h>


#define SendKey 0  //Button to send data Flash BTN on NodeMCU

int port = 8881;  //Port number
WiFiServer server(port);

//Server connect to WiFi Network
const char *ssid = "MOVISTAR_AA";  //Enter your wifi SSID
const char *password = "938332193";  //Enter your wifi Password

int count=0;
int temp=0;
int num=0;
char result[4]={0};




//=======================================================================
//                    Power on setup
//=======================================================================
void setup() 
{
  Serial.begin(115200);
  pinMode(SendKey,INPUT_PULLUP);  //Btn to send data
  Serial.println();

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password); //Connect to wifi
 
  // Wait for connection  
  Serial.println("Connecting to Wifi");
  while (WiFi.status() != WL_CONNECTED) {   
    delay(500);
    Serial.print(".");
    delay(500);
  }

  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);

  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());  
  server.begin();
  Serial.print("Open Telnet and connect to IP:");
  Serial.print(WiFi.localIP());
  Serial.print(" on port ");
  Serial.println(port);
  Serial.println("MAC Address:" + WiFi.macAddress());
}
//=======================================================================
//                    Loop
//=======================================================================

void loop() 

{
  char mytest;
  char mybuffer[4];
  String myString;
  String DataCompare="*TMP?";
  int index = 0;
  bool fetch;
  //int temp=0;
  WiFiClient client = server.available();
  
  if (client) {
    if(client.connected())
    {
      Serial.println("Client Connected");
    }
    
      while(client.connected()){      
      
        while(client.available()>0){
        // read data from the connected client
        
        //Serial.write(client.read()); 
        //Serial.print(" ---- ");
        
        char c = client.read();
        Serial.println(c);
        
        if (c == '*') {
          Serial.println("Open Season");
          fetch=true;
        }

          
        if(index <=4 and fetch==true)
        {
          mybuffer[index] = c;
          index++;
          Serial.println(index);
          //mybuffer[index] = '\0';
        }
        
        if(index>4 and fetch==true)
        {
          
          Serial.println(String(mybuffer));
          myString = String(mybuffer).substring(0,5);
          Serial.println(myString);
         
          index=0;       
          fetch=false;
          Serial.println("Season Closed");

          if (myString==DataCompare)
          {
            Serial.println("Client Write");
            //client.write("45");
            //client.write("1");

            
            num=num+1;
            if (num >= 10){
              num=1;
            }
            
            Serial.println(num);
            // Conversion to ASCII and storage in "result[4]"           
            Conv_Ascii(num);
            Serial.println(result);  // prints "345"
            client.write(result);                          
           }
         }         
      }
      //Serial.print("We are out");
        
      //Send Data to connected client
      while(Serial.available()>0)
      {
        //client.write(Serial.read());
        
      }
    }
    client.stop();
    Serial.println("Client disconnected");    
  }
}

//=======================================================================

void Conv_Ascii(int num){

            //char result[4] = {0};
            //int num = 1;

            //int digit = num % 10;  // get the 5 off the end
            //result[2] = digit + '0';  // convert to ascii and store
            //num /= 10;   // get rid of the 5
            //digit = num % 10;   // get the 4 off the end
            //result[1] = digit + '0';  // convert to ascii and store
            //num /= 10;   // get rid of the 4
            //int digit = num % 10;   // get the 3 off the end
            //result[0] = digit + '0';  // convert to ascii and store

            result[0] = num + '0';  // convert to ascii and store
            Serial.print(result);  // prints "345"
            //return result;
  
}
