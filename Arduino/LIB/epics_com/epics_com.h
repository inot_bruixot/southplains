
/*
  epics_com.h - Library for flashing epics_com code.
  Created by David A. Mellis, November 2, 2007.
  Released into the public domain.
*/
#ifndef epics_com_h
#define epics_com_h

#include "Arduino.h"
#include <WiFiUdp.h>

class epics_com
{
  public:
    epics_com(int pin);
	void dot();
    void dash();
	void BlinkLed(int freq);
	String Triage(char *udp_sFunRequest[],String sFunData_received,char *hwi_sFunField[]);
	void Send_udp_data(String sFunReply,  WiFiUDP UDP_Fun, int iFun_UDPPort_Target);
	String Parse_udp_data(WiFiUDP UDP_Fun);
	
	void Wifi_Conn(char *WIFI_SSID_Target_Fun[],char *WIFI_PASS_Target_Fun[]);
	int Wifi_Conf_UDP(String Fun_WIFI_MACs[],IPAddress Fun_WIFI_IPs[],int Fun_WIFI_Ports[], IPAddress Fun_Gateway, IPAddress Fun_Subnet);
	int Wifi_Conf_TCP(String Fun_WIFI_MACs[],IPAddress Fun_WIFI_IPs[],int Fun_WIFI_Ports[], IPAddress Fun_Gateway, IPAddress Fun_Subnet);
	void Wifi_CoRn_TCP(char *WIFI_SSID_Target_Fun[], char *WIFI_PASS_Target_Fun[]);
	void Wifi_ON_ServRer_TCP(int port, WiFiServer server_Fun);
	String * Parse_3_tcp_data(char c);
	
  private:
    int _pin;
};



#endif
