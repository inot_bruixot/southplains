#!../../bin/linux-arm/ESP8266_RBtest
#########!/home/inot/Desktop/IOCs/ESP8266/bin/linux-arm/ESP8266_RBtest

############################################################################### 
# Set up environment 
#< envPaths 
epicsEnvSet "TOP" "$(HOME)/Desktop/southplains/EPICS_Support/ESP8266_RB"
epicsEnvSet "STREAM_PROTOCOL_PATH" "$(TOP)/db" 

############################################################################### 
# Allow PV name prefixes and serial port name to be set from the environment 
epicsEnvSet "P" "$(P=BREWERY:)" 
epicsEnvSet "TTY" "$(TTY=/dev/ttyACM0)" 

############################################################################### 
## Register all support components 
cd "${TOP}" 
dbLoadDatabase "dbd/ESP8266_RBtest.dbd" 
ESP8266_RBtest_registerRecordDeviceDriver pdbbase 
 
############################################################################### 
# Set up ASYN ports 
# drvAsynSerialPortConfigure port ipInfo priority noAutoconnect noProcessEos 
drvAsynIPPortConfigure("L2", "192.168.1.22:4210:4022 UDP", 0, 0, 0, 0)

############################################################################### 
## Load record instances 
#dbLoadRecords("db/devESP8266_RB.db","P=$(P),R=Tank1:,A=L1")
dbLoadRecords("db/devESP8266_RB.db","P=$(P),R=Tank2:,A=L2")
#dbLoadRecords("db/devESP8266.db","P=$(P),R=Tank3:,A=L3")
#dbLoadRecords("db/devESP8266.db","P=$(P),R=Tank4:,A=L4")

####33########################################################################### 
## Start EPICS 
cd "${TOP}/iocBoot/${IOC}" 
iocInit 
#iocLogInit
