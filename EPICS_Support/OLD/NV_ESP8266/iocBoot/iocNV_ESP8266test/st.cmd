#!../../bin/linux-x86_64/NV_ESP8266test
#########!/home/inot/Desktop/IOCs/ESP8266/bin/linux-x86_64/NV_ESP8266test

############################################################################### 
# Set up environment 
#< envPaths 
epicsEnvSet "TOP" "$(HOME)/Desktop/southplains/EPICS_Support/NV_ESP8266"
epicsEnvSet "STREAM_PROTOCOL_PATH" "$(TOP)/db" 

############################################################################### 
# Allow PV name prefixes and serial port name to be set from the environment 
epicsEnvSet "P" "$(P=BREWERY:)" 
epicsEnvSet "TTY" "$(TTY=/dev/ttyACM0)" 

############################################################################### 
## Register all support components 
cd "${TOP}" 
dbLoadDatabase "dbd/NV_ESP8266test.dbd" 
NV_ESP8266test_registerRecordDeviceDriver pdbbase 
 
############################################################################### 
# Set up ASYN ports 
# drvAsynSerialPortConfigure port ipInfo priority noAutoconnect noProcessEos 
#drvAsynIPPortConfigure("L2", "192.168.1.22:4210:4022 UDP", 0, 0, 0, 0)
#drvAsynIPPortConfigure("L3", "192.168.1.30:4210:4030 UDP", 0, 0, 0, 0)

#drvAsynIPPortConfigure("L2", "192.168.1.21:4210:4022 UDP", 0, 0, 0, 0)

drvAsynIPPortConfigure("L6", "192.168.1.36:8886:8886 TCP", 0, 0, 0, 0)
drvAsynIPPortConfigure("L7", "192.168.1.37:8887:8887 TCP", 0, 0, 0, 0)



############################################################################### 
## Load record instances 
#dbLoadRecords("db/devESP8266.db","P=$(P),R=Tank1:,A=L1")

#dbLoadRecords("db/devNV_ESP8266.db","P=$(P),R=Tank2:,A=L2")

dbLoadRecords("db/devNV_ESP8266.db","P=$(P),R=Tank6:,A=L6")
dbLoadRecords("db/devNV_ESP8266.db","P=$(P),R=Tank7:,A=L7")

#dbLoadRecords("db/devESP8266.db","P=$(P),R=Tank3:,A=L3")
#dbLoadRecords("db/devESP8266.db","P=$(P),R=Tank4:,A=L4")

####33########################################################################### 
## Start EPICS 
cd "${TOP}/iocBoot/${IOC}" 
iocInit 
#iocLogInit