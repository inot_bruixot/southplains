#!../../bin/linux-x86_64/nano33IoTtest

#- You may have to change nano33IoTtest to something else
#- everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/nano33IoTtest.dbd"
nano33IoTtest_registerRecordDeviceDriver pdbbase

## Load record instances
#dbLoadRecords("db/xxx.db","user=iocuser")

cd "${TOP}/iocBoot/${IOC}"
iocInit

## Start any sequence programs
#seq sncxxx,"user=iocuser"
