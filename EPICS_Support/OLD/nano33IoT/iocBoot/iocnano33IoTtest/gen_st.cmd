#!../../bin/linux-x86_64/nano33IoTtest


############################################################################### 
# Set up environment 
< envPaths 
epicsEnvSet "STREAM_PROTOCOL_PATH" "$(TOP)/db" 

 
############################################################################### 
# Allow PV name prefixes and serial port name to be set from the environment 
epicsEnvSet "P" "$(P=BWY)" 
epicsEnvSet "D1" "$(D1=Tank)" 
epicsEnvSet "D2" "$(D2=Heater)" 
epicsEnvSet "R" "$(R=NotUsed)" 

#epicsEnvSet "TTY" "$(TTY=/dev/tty.PL2303-000013FA)" 
#epicsEnvSet "TTY" "$(TTY=/dev/ttyACM0)" 
epicsEnvSet "TTY" "$(TTY=/dev/my_uart)" 

 
############################################################################### 
## Register all support components 
cd "${TOP}" 
dbLoadDatabase "dbd/nano33IoTtest.dbd"
nano33IoTtest_registerRecordDeviceDriver pdbbase

 
############################################################################### 
# Set up ASYN ports 
# drvAsynSerialPortConfigure port ipInfo priority noAutoconnect noProcessEos 
drvAsynSerialPortConfigure("L0","$(TTY)",0,0,0) 
asynSetOption("L0", -1, "baud", "9600") 
asynSetOption("L0", -1, "bits", "8") 
asynSetOption("L0", -1, "parity", "none") 
asynSetOption("L0", -1, "stop", "2") 
asynSetOption("L0", -1, "clocal", "Y") 
asynSetOption("L0", -1, "crtscts", "Y") 
asynOctetSetInputEos("L0", -1, "\n") 
asynOctetSetOutputEos("L0", -1, "\n") 
asynSetTraceIOMask("L0",-1,0x2) 
asynSetTraceMask("L0",-1,0x9) 

 
############################################################################### 
## Load record instances 
#dbLoadRecords("db/devnano33IoT.db","P=$(P),R=$(R),PORT=L0,A=0") 
dbLoadRecords("db/asicpython.db","P=$(P),R=$(R),D1=$(D1),D2=$(D2),PORT=L0,A=0") 


############################################################################### 
## Start EPICS 
cd "${TOP}/iocBoot/${IOC}" 
iocInit 
  






#- You may have to change nano33IoTtest to something else
#- everywhere it appears in this file

#< envPaths

#cd "${TOP}"

## Register all support components
#dbLoadDatabase "dbd/nano33IoTtest.dbd"
#nano33IoTtest_registerRecordDeviceDriver pdbbase

## Load record instances
#dbLoadRecords("db/xxx.db","user=iocuser")

#cd "${TOP}/iocBoot/${IOC}"
#iocInit

## Start any sequence programs
#seq sncxxx,"user=iocuser"

