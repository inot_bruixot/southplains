#!../../bin/linux-x86_64/ESP8266_VMtest

#- You may have to change ESP8266_VMtest to something else
#- everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/ESP8266_VMtest.dbd"
ESP8266_VMtest_registerRecordDeviceDriver pdbbase

## Load record instances
#dbLoadRecords("db/xxx.db","user=inot")

cd "${TOP}/iocBoot/${IOC}"
iocInit

## Start any sequence programs
#seq sncxxx,"user=inot"
