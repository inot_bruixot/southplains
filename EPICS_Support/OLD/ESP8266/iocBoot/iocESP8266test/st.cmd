#!../../bin/linux-x86_64/ESP8266test
#########!/home/inot/Desktop/IOCs/ESP8266/bin/linux-x86_64/ESP8266test

############################################################################### 
# Set up environment 
#< envPaths 
epicsEnvSet "TOP" "$(HOME)/Desktop/southplains/EPICS_Support/ESP8266"
epicsEnvSet "STREAM_PROTOCOL_PATH" "$(TOP)/db" 
#epicsEnvSet "STREAM_PROTOCOL_PATH" "/home/inot/Desktop/IOCs/ESP8266/db" 
 
############################################################################### 
# Allow PV name prefixes and serial port name to be set from the environment 
epicsEnvSet "P" "$(P=BREWERY:)" 
#epicsEnvSet "R" "$(R=Tank)" 
#epicsEnvSet "R2" "$(R=Tank2)" 
#epicsEnvSet "TTY" "$(TTY=/dev/tty.PL2303-000013FA)" 
epicsEnvSet "TTY" "$(TTY=/dev/ttyACM0)" 

 
############################################################################### 
## Register all support components 
cd "${TOP}" 
dbLoadDatabase "dbd/ESP8266test.dbd" 
ESP8266test_registerRecordDeviceDriver pdbbase 

 
############################################################################### 
# Set up ASYN ports 
# drvAsynSerialPortConfigure port ipInfo priority noAutoconnect noProcessEos 
#drvAsynSerialPortConfigure("L0","$(TTY)",0,0,0) 
#drvAsynIPPortConfigure("OC_CMD", "192.168.1.229:4210 UDP*", 0, 0, 0)
#drvAsynIPPortConfigure("OC_CMD", "192.168.1.229:4210 UDP*", 0, 0, 0)
#asynSetOption("L0", -1, "baud", "9600") 
#asynSetOption("L0", -1, "bits", "8") 
#asynSetOption("L0", -1, "parity", "none") 
#asynSetOption("L0", -1, "stop", "2") 
#asynSetOption("L0", -1, "clocal", "Y") 
#asynSetOption("L0", -1, "crtscts", "Y") 
#asynOctetSetInputEos("L0", -1, "\n") 
#asynOctetSetOutputEos("L0", -1, "\n") 
#asynSetTraceIOMask("L0",-1,0x2) 
#asynSetTraceMask("L0",-1,0x9) 

#drvAsynIPPortConfigure("OC_CMD", "192.168.1.28:4210 UDP*", 0, 0, 0)
#drvAsynIPPortConfigure("L0", "192.168.1.28:4210 UPD*", 0, 0, 0, 0)
#drvAsynIPPortConfigure("L0", "192.168.1.28:4210", 0, 0, 0, 0)
#drvAsynIPPortConfigure("L0", "192.168.1.28:45947 UDP*", 0, 0, 0, 0)
#drvAsynIPPortConfigure("L0", "192.168.1.96:4210 UDP", 0, 0, 0, 0)

#drvAsynIPPortConfigure("portName","hostInfo",priority,noAutoConnect,noProcessEos)
#drvAsynIPPortConfigure("L0", "192.168.1.28:4210 UDP", 0, 0, 0, 0)

#drvAsynIPServerPortConfigure("portName", "serverInfo", maxClients, priority, noAutoConnect, noProcessEos);
#drvAsynIPServerPortConfigure("L0", "192.168.1.28:4210 UDP", 2,0, 0, 0);


#drvAsynIPServerPortConfigure("L0", "192.168.1.211:4211 UDP", 2,0, 0, 0);
#drvAsynIPPortConfigure("L0", "192.168.1.28:4210:4211 UDP", 0, 0, 0, 0)
#drvAsynIPPortConfigure("L1", "192.168.1.29:4210:4210 UDP", 0, 0, 0, 0)

##drvAsynIPPortConfigure("L0", "192.168.1.177:4210:4210 UDP", 0, 0, 0, 0)
######drvAsynIPPortConfigure("L1", "192.168.1.28:4211:4211 UDP", 0, 0, 0, 0)
#drvAsynIPPortConfigure("L1", "192.168.88.21:4210:4021 UDP", 0, 0, 0, 0)
drvAsynIPPortConfigure("L2", "192.168.1.22:4210:4022 UDP", 0, 0, 0, 0)
#drvAsynIPPortConfigure("L3", "192.168.1.203:4210:4203 UDP", 0, 0, 0, 0)
#drvAsynIPPortConfigure("L4", "192.168.88.24:4210:4024 UDP", 0, 0, 0, 0)

#drvAsynIPPortConfigure("L2", "192.168.1.202:4210:4202 UDP", 0, 0, 0, 0)

############################################################################### 
## Load record instances 
#dbLoadRecords("db/devESP8266.db","P=$(P),R=$(R),PORT=4210,A=0") 
#dbLoadRecords("db/basicpython.db","P=$(P),R=$(R),PORT=4210,A=0") 


########dbLoadRecords("db/devESP8266.db","P=$(P),R=Tank:,PORT=4210,A=L0")
#######dbLoadRecords("db/devESP8266.db","P=$(P),R=Tank2:,PORT=4211,A=L1")
dbLoadRecords("db/devESP8266.db","P=$(P),R=Tank1:,A=L1")
dbLoadRecords("db/devESP8266.db","P=$(P),R=Tank2:,A=L2")
#dbLoadRecords("db/devESP8266.db","P=$(P),R=Tank3:,A=L3")
dbLoadRecords("db/devESP8266.db","P=$(P),R=Tank4:,A=L4")



####33########################################################################### 
## Start EPICS 
cd "${TOP}/iocBoot/${IOC}" 
iocInit 
iocLogInit