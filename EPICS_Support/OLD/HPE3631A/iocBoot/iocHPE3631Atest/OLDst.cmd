#!../../bin/linux-x86_64/HPE3631Atest

#- You may have to change HPE3631Atest to something else
#- everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/HPE3631Atest.dbd"
HPE3631Atest_registerRecordDeviceDriver pdbbase

## Load record instances
#dbLoadRecords("db/xxx.db","user=inot")

cd "${TOP}/iocBoot/${IOC}"
iocInit

## Start any sequence programs
#seq sncxxx,"user=inot"
