#!../../bin/linux-x86_64/HPE3631Atest

############################################################################### 
# Set up environment 
< envPaths 
epicsEnvSet "STREAM_PROTOCOL_PATH" "$(TOP)/db" 

 
############################################################################### 
# Allow PV name prefixes and serial port name to be set from the environment 
epicsEnvSet "P" "$(P=hpE3631A)" 
epicsEnvSet "R" "$(R=Test)" 
#epicsEnvSet "TTY" "$(TTY=/dev/tty.PL2303-000013FA)" 
#epicsEnvSet "TTY" "$(TTY=/dev/ttyACM0)" 
epicsEnvSet "TTY" "$(TTY=/dev/ttyUSB0)" 

 
############################################################################### 
## Register all support components 
cd "${TOP}" 
dbLoadDatabase "dbd/HPE3631Atest.dbd" 
HPE3631Atest_registerRecordDeviceDriver pdbbase 

 
############################################################################### 
# Set up ASYN ports 
# drvAsynSerialPortConfigure port ipInfo priority noAutoconnect noProcessEos 
drvAsynSerialPortConfigure("L0","$(TTY)",0,0,0) 
#asynSetOption("L0", -1, "baud", "9600") 
asynSetOption("L0", -1, "baud", "115200") 
asynSetOption("L0", -1, "bits", "8") 
asynSetOption("L0", -1, "parity", "none") 
asynSetOption("L0", -1, "stop", "2") 
asynSetOption("L0", -1, "clocal", "Y") 
asynSetOption("L0", -1, "crtscts", "Y") 
asynOctetSetInputEos("L0", -1, "\n") 
asynOctetSetOutputEos("L0", -1, "\n") 
asynSetTraceIOMask("L0",-1,0x2) 
asynSetTraceMask("L0",-1,0x9) 

 
############################################################################### 
## Load record instances 
dbLoadRecords("db/devHPE3631A.db","P=$(P),R=$(R),PORT=L0,A=0") 


############################################################################### 
## Start EPICS 
cd "${TOP}/iocBoot/${IOC}" 
iocInit 
  